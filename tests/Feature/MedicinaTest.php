<?php

namespace Tests\Feature;

use App\Models\Medicina;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class MedicinaTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_index_libro_medicinas()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    public function test_create_medicinas()
    {
        $data = [
            'nombre' => 'bax',
            'descripcion' => '100ml',
            'stock' => 10,
            'precio' => 5.50,
            'via' => 'oral'
        ];
        $response = $this->post('/medicinas', $data);
        $response->assertStatus(302);
    }

    public function test_update_medicinas()
    {
        // $this->withoutExceptionHandling();

        $medicina = Medicina::factory()->create();

        $response = $this->put("/medicinas/$medicina->id", [
            'nombre' => 'bax',
            'descripcion' => '100ml',
            'stock' => 10,
            'precio' => 5.50,
            'via' => 'oral'
        ]);
        $response->assertStatus(302);
    }



    public function test_delete_medicinas()
    {


        $medicina = Medicina::factory()->create();

        $response = $this->delete("/medicinas/$medicina->id");

        $response->assertStatus(302);
    }
}
