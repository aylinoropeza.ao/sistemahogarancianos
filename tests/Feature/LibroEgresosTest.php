<?php

namespace Tests\Feature;

use App\Models\LibroEgreso;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Tests\Feature\WithStubUser;
use Illuminate\Foundation\Testing\DatabaseTransactions;


class LibroEgresosTest extends TestCase
{
    use DatabaseTransactions;
    /**
     * A basic feature test example.
     *
     * @return void
     */

    public function test_index_libro_egresos()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }
    
    public function test_create_libro_egresos()
    {
        $data = ['fecha' => '2023-11-1',
        'concepto' => '2',
        'comentario' => '3',
        'saldo' => 5.50];
        $response = $this->post('/libroEgresos',$data);
        $response->assertStatus(302);
        
    }
    
    public function test_update_libro_egresos()
    {
       // $this->withoutExceptionHandling();
        
        $libroEgreso=LibroEgreso::factory()->create();

        $response = $this->put("/libroEgresos/$libroEgreso->id", [
            'fecha' => '2023-11-1',
            'concepto' => '2',
            'comentario' => '3',
            'saldo' => 5.50
        ]);
        $response->assertStatus(302);
    }


    
    public function test_delete_libro_egresos(){

     
        $libroEgreso = LibroEgreso::factory()->create();
        
        $response = $this->delete("/libroEgresos/$libroEgreso->id");

        $response->assertStatus(302);
        

    }

}
