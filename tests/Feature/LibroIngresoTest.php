<?php

namespace Tests\Feature;

use App\Models\LibroIngreso;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class LibroIngresoTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_index_libro_ingresos()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    public function test_create_libro_ingresos()
    {
        $data = [
            'fecha' => '2023-11-1',
            'concepto' => 'Reparacion',
            'comentario' => 'ingreso por donacion',
            'saldo' => 5.50,
            'tipo' => 'donacion'
        ];
        $response = $this->post('/libroIngresos', $data);
        $response->assertStatus(302);
    }

    public function test_update_libro_ingresos()
    {
        // $this->withoutExceptionHandling();

        $libroIngreso = LibroIngreso::factory()->create();

        $response = $this->put("/libroIngresos/$libroIngreso->id", [
            'fecha' => '2023-11-1',
            'concepto' => 'Reparacion',
            'comentario' => 'ingreso por donacion',
            'saldo' => 5.50,
            'tipo' => 'donacion'
        ]);
        $response->assertStatus(302);
    }



    public function test_delete_ingresos()
    {


        $libroIngreso = LibroIngreso::factory()->create();

        $response = $this->delete("/libroIngresos/$libroIngreso->id");

        $response->assertStatus(302);
    }
}
