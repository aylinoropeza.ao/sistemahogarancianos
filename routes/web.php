<?php

use App\Http\Controllers\HistorialeController;
use App\Http\Controllers\ResidenteController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Auth::routes();

Route::get('/home', 'App\Http\Controllers\HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function () {
	Route::resource('user', 'App\Http\Controllers\UserController', ['except' => ['show']]);
	Route::get('profile', ['as' => 'profile.edit', 'uses' => 'App\Http\Controllers\ProfileController@edit']);
	Route::put('profile', ['as' => 'profile.update', 'uses' => 'App\Http\Controllers\ProfileController@update']);
	Route::get('upgrade', function () {return view('pages.upgrade');})->name('upgrade'); 
	Route::get('map', function () {return view('pages.maps');})->name('map');
	Route::get('icons', function () {return view('pages.icons');})->name('icons');

	Route::resource('/residentes', 'App\Http\Controllers\ResidenteController');
	Route::resource('/empleados', 'App\Http\Controllers\EmpleadoController');
	Route::get('/residentes/{residente}/pdf', [App\Http\Controllers\ResidenteController::class,'getPdfSignosVitales']);
	Route::get('/residentes/{residente}/hijos/create', function ($residente_id) {return view('hijos.create');});
	Route::resource('/hijos', 'App\Http\Controllers\HijoController');
	Route::resource('/medicinas', 'App\Http\Controllers\MedicinaController');
	Route::resource('/tratamientos', 'App\Http\Controllers\TratamientoController');
	Route::get('/tratamientos/{id}/create', [\App\Http\Controllers\TratamientoController::class, 'create'])->name('tratamientos.create');
	Route::resource('/libroIngresos', 'App\Http\Controllers\LibroIngresoController');
	Route::resource('/libroEgresos', 'App\Http\Controllers\LibroEgresoController');
	Route::resource('/libroGastos', 'App\Http\Controllers\LibroGastoController');
	Route::resource('/dietas', 'App\Http\Controllers\DietaController');
	Route::get('/dietas/{id}/create', [\App\Http\Controllers\DietaController::class, 'create'])->name('dietas.create');

	Route::get('table-list', function () {return view('pages.tables');})->name('table');
	Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'App\Http\Controllers\ProfileController@password']);

	//libro Ingresos
	Route::resource('/libroIngresos', 'App\Http\Controllers\LibroIngresoController');
	//libro Egresos
	Route::resource('/libroEgresos', 'App\Http\Controllers\LibroEgresoController');
	//historiales
	Route::resource('/residentes/{residente}/historiales', 'App\Http\Controllers\HistorialeController');

	Route::delete('/historiales/{id}', [\App\Http\Controllers\HistorialeController::class, 'destroye'])->name('historiales.destroye'); // Laravel 8
	Route::get('/historiales/{id}/edite', [\App\Http\Controllers\HistorialeController::class, 'edite'])->name('historiales.edite'); // Laravel 8
	Route::put('/historiales/{id}', [\App\Http\Controllers\HistorialeController::class, 'updates'])->name('historiales.updates');
	
	//infromes 
	Route::get('/informes', [App\Http\Controllers\InformeController::class,'index'])->name('informes.index');

	Route::post('/informes/pdfIngresosFecha', [App\Http\Controllers\InformeController::class,'pdfIngresosFecha'])->name('informes.pdfIngresosFecha');

	Route::post('/informes/pdfGastosResidente', [App\Http\Controllers\InformeController::class,'pdfGastosResidente'])->name('informes.pdfGastosResidente');

	Route::get('/informes/pdfIngresos', [App\Http\Controllers\InformeController::class,'pdfIngresos']);
	
	Route::get('/informes/pdfEgresos', [App\Http\Controllers\InformeController::class,'pdfEgresos']);
	Route::get('/informes/pdfTotal', [App\Http\Controllers\InformeController::class,'pdfTotal']);
	
});

