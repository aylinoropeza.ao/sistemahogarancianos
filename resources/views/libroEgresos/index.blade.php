@extends('layouts.app')

@section('content')
  @include('layouts.headers.header')
  <!-- Tabla -->
  <div class="container-fluid mt--6">
    <div class="pb-2 pr-1 text-right"><a href="libroEgresos/create" class="btn btn-sm btn-neutral">Ingresar elemento</a></div>
    <div class="row">
        <div class="col">
          <div class="card bg-default shadow">
            <div class="card-header bg-transparent border-0">
              <div class="row responsive">
                <div class="col-8"><h3 class="text-white mb-0">Lista elementos del Libro de Egresos</h3></div>
              </div>
            </div>
            <div class="table-responsive">
              <table class="table align-items-center table-dark table-flush">
                <thead class="thead-dark">
                  <tr>
                    <th scope="col" class="sort">Id</th>
                    <th scope="col" class="sort" data-sort="name">Fecha</th>
                    <th scope="col" class="sort">Concepto</th>
                    <th scope="col" class="sort">Comentario</th>
                    <th scope="col" class="sort">Saldo</th>
                    <th scope="col"></th>
                  </tr>
                </thead>
                <tbody class="list">
                  @foreach ($libroEgresos as $libroEgreso)
                    <tr>
                      <td>{{$libroEgreso->id}}</td>
                      <td>{{$libroEgreso->fecha}}</td>
                      <td>{{$libroEgreso->concepto}}</td>
                      <td>{{$libroEgreso->comentario}}</td>
                      <td>{{$libroEgreso->saldo}}</td>
                      <td class="text-right">
                        <div class="dropdown">
                          <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-ellipsis-v"></i>
                          </a>
                          <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                            <a class="dropdown-item" href="libroEgresos/{{$libroEgreso->id}}/edit">Editar</a>
                            <form id="{{$libroEgreso->id}}" action="{{route('libroEgresos.destroy', $libroEgreso->id)}}" method="POST">
                              @csrf
                              @method('DELETE')
                              <a class="dropdown-item" href="javascript:{}" onclick="document.getElementById('{{$libroEgreso->id}}').submit(); return false;">Eliminar</a>
                            </form>
                          </div>
                        </div>
                      </td>
                    </tr>
                @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
    </div>
  </div>
@endsection

@push('js')
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.extension.js"></script>
@endpush