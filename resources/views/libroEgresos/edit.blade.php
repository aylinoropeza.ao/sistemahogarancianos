@extends('layouts.app', ['title' => __('Editar lemento Libro de Egreso')])

@section('content')
  @include('layouts.headers.header')
  <div class="container-fluid mt--7">
    <div class="card bg-secondary shadow">
      <div class="card-header bg-white border-0">
        <div class="row align-items-center">
          <h3 class="mb-0">{{ __('Editar libro Egreso') }}</h3>
        </div>
      </div>
      <div class="card-body">
        <form method="post" action="/libroEgresos/{{$libroEgreso->id}}" autocomplete="off">
          @csrf
          @method('put')
          <h6 class="heading-small text-muted mb-4">{{ __('Información del elemento Libro de Egreso') }}</h6>
                                
          <div class="pl-lg-4">
            <div class="form-group">
              <label class="form-control-label" for="fecha">{{ __('Fecha') }}</label>
              <input type="date" name="fecha" id="fecha" class="form-control" value="{{$libroEgreso->fecha}}" required autofocus>
            </div>
            <div class="form-group">
              <label class="form-control-label" for="concepto">{{ __('Concepto') }}</label>
              <input type="text" name="concepto" id="concepto" class="form-control" value="{{$libroEgreso->concepto}}" required>
            </div>
            <div class="form-group">
              <label class="form-control-label" for="comentario">{{ __('Comentario') }}</label>
              <input type="text" name="comentario" id="comentario" class="form-control" value="{{$libroEgreso->comentario}}" required>
            </div>
            <div class="form-group">
              <label class="form-control-label" for="saldo">{{ __('Saldo') }}</label>
              <input type="number" name="saldo" id="saldo" class="form-control" value="{{$libroEgreso->saldo}}" required>
            </div>
            

            <div class="text-center">
              <a href="/libroEgresos" class="btn btn-secondary mt-4">{{ __('Cancelar') }}</a>
              <button type="submit" class="btn btn-success mt-4">{{ __('Guardar') }}</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
@endsection

@push('js')
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.extension.js"></script>
@endpush