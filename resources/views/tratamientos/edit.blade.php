@extends('layouts.app', ['title' => __('Editar tratamiento')])

@section('content')
  @include('layouts.headers.header')
  <div class="container-fluid mt--7">
    <div class="card bg-secondary shadow">
      <div class="card-header bg-white border-0">
        <div class="row align-items-center">
          <h3 class="mb-0">{{ __('Editar tratamiento') }}</h3>
        </div>
      </div>
      <div class="card-body">
        <form method="post" action="/tratamientos/{{$tratamiento->id}}" autocomplete="off">
          @csrf
          @method('put')
          <h6 class="heading-small text-muted mb-4">{{ __('Ingresar datos') }}</h6>
                                
          <div class="pl-lg-4">
            <div class="form-group">
              <label class="form-control-label" for="descripcion">{{ __('Descripción') }}</label>
              <input type="text" name="descripcion" id="descripcion" class="form-control" value="{{$tratamiento->descripcion}}" required autofocus>
            </div>
            <div class="form-group">
              <label class="form-control-label" for="hora">{{ __('Hora') }}</label>
              <input type="time" name="hora" id="hora" class="form-control" value="{{$tratamiento->hora}}" required>
            </div>
            <div class="form-group">
              <label class="form-control-label" for="fecha">{{ __('Fecha') }}</label>
              <input type="date" name="fecha" id="fecha" class="form-control" value="{{$tratamiento->fecha}}" required>
            </div>
            <div class="form-group">
              <label class="form-control-label" for="medicina_id">{{ __('Medicamento') }}</label>
              <select class="form-control select2" id="medicina_id" name="medicina_id">
                @foreach($medicinas as $medicina)
                  <option value="{{$medicina->id}}" @if($tratamiento->medicina_id == $medicina->id) selected @endif>{{$medicina->nombre}} ({{$medicina->descripcion}})</option>
                @endforeach
              </select>
            </div>
            <div class="form-group">
              <label class="form-control-label" for="empleado_id">{{ __('Enfermera(o)') }}</label>
              <select class="form-control select2" id="empleado_id" name="empleado_id">
                @foreach($empleados as $empleado)
                  <option value="{{$empleado->id}}" @if($tratamiento->empleado_id == $empleado->id) selected @endif>{{$empleado->nombre}} {{$empleado->apellido}}</option>
                @endforeach
              </select>
            </div>
            <div class="form-group">
              <label class="form-control-label" for="dosis">{{ __('Dosis') }}</label>
              <input type="text" name="dosis" id="dosis" class="form-control" value="{{$tratamiento->dosis}}" required>
            </div>
            <input type="hidden" name="historial_id" value="{{$tratamiento->historial_id}}">
            <div class="text-center">
              <a href="../../tratamientos/{{$residente->id}}" class="btn btn-secondary mt-4">{{ __('Cancelar') }}</a>
              <button type="submit" class="btn btn-success mt-4">{{ __('Guardar') }}</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
@endsection

@push('js')
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.extension.js"></script>
@endpush