@extends('layouts.app')

@section('content')
  @include('layouts.headers.header')
  <!-- Tabla -->
  <div class="container-fluid mt--6">
    <div class="row">
        <div class="col-xl">
                <div class="card card-profile shadow">
                    <div class="row justify-content-center">
                        <div class="col-lg-3 order-lg-2">
                            <div class="card-profile-image">
                                <a href="#">
                                    {{--@if($residente->imagen === null)--}}
                                      <img src="{{ asset('img') }}/residente/img.jpg" class="rounded-circle">
                                    {{--@else
                                      <img src="{{ asset('img') }}/residente/{$residente->imagen}}" class="rounded-circle">
                                    @endif--}}
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="card-header text-center border-0 pt-8 pt-md-4 pb-0 pb-md-4">
                        <div class="d-flex justify-content-between">
                            <a href="/residentes/{{$residente->id}}/historiales" class="btn btn-sm btn-info mr-4">{{ __('Volver a historial') }}</a>
                            <div class="row mr-4">
                            <a href="../tratamientos/{{$tratamientos->first()->historial_id}}/create" class="btn btn-sm btn-info ">{{ __('Insertar') }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body pt-0 pt-md-4">
                        <div class="row">
                            <div class="col">
                                <div class="card-profile-stats d-flex justify-content-center mt-md-5">
                                    <div>
                                        <span class="description">{{ __('Nombre') }}</span>
                                        <span class="heading">{{$residente->nombre}}</span>
                                    </div>
                                    <div>
                                        <span class="description">{{ __('Apellido') }}</span>
                                        <span class="heading">{{$residente->apellido}}</span>
                                    </div>
                                    <div>
                                        <span class="description">{{ __('Edad') }}</span>
                                        <span class="heading">{{$edad}}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{--dd($residente, $tratamiento, $medicinas[0])--}}
                        <h1 class="text-center">Tratamientos</h2>
                        @foreach($tratamientos as $tratamiento)
                        <hr />
                        <div>
                          <div class="row mx-5">
                            <div class="col">
                              <h3>Descripción</h3>
                              <p>{{$tratamiento->descripcion}}</p>
                            </div>
                          </div>
                          <div class="row mx-5">
                            <div class="col">
                              <h3>Fecha</h3>
                              <p>{{$tratamiento->fecha}}</p>
                            </div>
                            <div class="col">
                              <h3>Hora</h3>
                              <p>{{$tratamiento->hora}}</p>
                            </div>
                            <div class="col">
                              <h3>Medicación</h3>
                              <p>{{$medicina->nombre}}</p>
                            </div>
                            <div class="col ">
                              <h3>Vía</h3>
                              <p>{{$medicina->via}}</p>
                            </div>
                            <div class="col ">
                              <h3>Dosis</h3>
                              <p>{{$tratamiento->dosis}}</p>
                            </div>
                            <div class="col-1 dropdown">
                              <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-ellipsis-v"></i>
                              </a>
                              <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                <a href="/tratamientos/{{$tratamiento->id}}/edit" class="dropdown-item">{{ __('Editar') }}</a>
                                <form id="{{$tratamiento->id}}" action="{{route('tratamientos.destroy', $tratamiento->id)}}" method="POST">
                                  @csrf
                                  @method('DELETE')
                                  <a class="dropdown-item" href="javascript:{}" onclick="document.getElementById('{{$tratamiento->id}}').submit(); return false;">Eliminar</a>
                                </form>
                              </div>
                            </div>
                          </div>
                        </div>
                        @endforeach
                    </div>
                </div>
        </div>
    </div>
  </div>
@endsection

@push('js')
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.extension.js"></script>
@endpush