<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">

    <title></title>
    <style>
        table {
            border-collapse: collapse;
            width: 100%;
        }

        th,
        td {
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #f2f2f2
        }

        th {
            background-color: #04AA6D;
            color: white;
        }

        div {
            padding: 70px;
            border: 1px solid grey;
            border-radius: 5px;
        }

    </style>
</head>
<body>
    <h4>Nombre : {{ $signosVitales[0]->nombre }} {{ $signosVitales[0]->apellido}}  CI : {{ $signosVitales[0]->ci }}</h4>
    <table >
        <thead>
            <tr>
                <th>Fecha</th>
                <th>P/A</th>
                <th>F/C</th>
                <th>T</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($signosVitales as $signosVitale)
                <tr>
                    <td>{{ $signosVitale->fecha}}/{{ $signosVitale->hora}}</td>
                    <td>{{ $signosVitale->presionArterial }}</td>
                    <td>{{ $signosVitale->frecuenciaCardiaca}}</td>
                    <td>{{ $signosVitale->temperatura}}°</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</body>
</html>