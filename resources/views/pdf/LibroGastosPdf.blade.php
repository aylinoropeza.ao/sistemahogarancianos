<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">

    <title></title>
    <style>
        table {
            border-collapse: collapse;
            width: 100%;
        }

        th,
        td {
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #f2f2f2
        }

        th {
            background-color: #04AA6D;
            color: white;
        }

        div {
            padding: 70px;
            border: 1px solid grey;
            border-radius: 5px;
        }

    </style>
</head>
<body>
    <h4>Informe de Gastos </h4>
    <h4>Nombre : {{ $residente->nombre }} Apellido : {{ $residente->apellido }} </h4>
    <h4>CI : {{ $residente->ci }} </h4>
    <h4>Total saldo :  {{ $saldo }}</h4>
    <table >
        <thead>
            <tr>
                <th>Fecha</th>
                <th>Concepto</th>
                <th>Estado Pagado</th>
                <th>Saldo</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($LibroGastos as $libroGasto)
            <tr>
                <td>{{ $libroGasto->fecha}}</td>
                <td>{{ $libroGasto->descripcion }}</td>
                @if ($libroGasto->estado_pagado == 1)
                    <td style="color:#1e531e"> cancelado</td>
                @else
                    <td style="color:#ff0000"> pendiente</td>
                @endif
                <td>{{ $libroGasto->precio}}</td>
            </tr>
        @endforeach
        <tr>
            <td></td>
            <td></td>
            <td>Total:</td>
            <td>{{ $saldo}}</td>
        </tr>
        </tbody>
    </table>
</body>
</html>