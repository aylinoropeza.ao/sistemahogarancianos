<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">

    <title></title>
    <style>
        table {
            border-collapse: collapse;
            width: 100%;
        }

        th,
        td {
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #f2f2f2
        }

        th {
            background-color: #04AA6D;
            color: white;
        }

        div {
            padding: 70px;
            border: 1px solid grey;
            border-radius: 5px;
        }

    </style>
</head>
<body>
    <h4>Informe Libro Egresos</h4>
    <h4>Total saldo :  {{ $suma }}</h4>
    <table >
        <thead>
            <tr>
                <th>Fecha</th>
                <th>Concepto</th>
                <th>Comentario</th>
                <th>Saldo</th>
            </tr>
        </thead>
        <tbody>
            
                
           
            @foreach ($libroEgresos as $libroEgreso)
            <tr>
                <td>{{ $libroEgreso->fecha}}</td>
                <td>{{ $libroEgreso->concepto }}</td>
                <td>{{ $libroEgreso->comentario}}</td>
                <td>{{ $libroEgreso->saldo}}</td>
            </tr>
            @endforeach
            @foreach ($libroIngresos as $libroIngreso)
            <tr>
                <td>{{ $libroIngreso->fecha}}</td>
                <td>{{ $libroIngreso->concepto }}</td>
                <td>{{ $libroIngreso->comentario}}</td>
                <td>{{ $libroIngreso->saldo}}</td>
            </tr>
            @endforeach
         
        <tr>
            <td></td>
            <td></td>
            <td>Total:</td>
            <td>{{ $suma}}</td>
        </tr>
        </tbody>
    </table>
</body>
</html>