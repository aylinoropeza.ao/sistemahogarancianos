@extends('layouts.app', ['title' => __('Ingresar historial')])

@section('content')
  @include('layouts.headers.header')
  <div class="container-fluid mt--7">
    <div class="card bg-secondary shadow">
      <div class="card-header bg-white border-0">
        <div class="row align-items-center">
          <h3 class="mb-0">{{ __('Ingresar historial') }}</h3>
        </div>
      </div>
      <div class="card-body">
        <form method="post" action="/residentes/{{$residente->id}}/historiales" autocomplete="off">
          @csrf

          <h6 class="heading-small text-muted mb-4">{{ __('Información del Historial') }}</h6>
                                
          <div class="pl-lg-4">
            <div class="form-group">
              <label class="form-control-label" for="descripcion_enfermedad">{{ __('descripcion_enfermedad') }}</label>
              <input type="text" name="descripcion_enfermedad" id="descripcion_enfermedad" class="form-control" value="{{ old('descripcion_enfermedad') }}" required>
            </div>
            @if ($errors->has('descripcion_enfermedad'))
              <span class="error text-danger" for="descripcion_enfermedad">{{ $errors->first('descripcion_enfermedad') }}</span>
            @endif

            <div class="form-group">
              <label class="form-control-label" for="fecha">{{ __('Fecha') }}</label>
              <input type="date" name="fecha" id="fecha" class="form-control" value="{{ old('fecha') }}" required>
            </div>
            @if ($errors->has('fecha'))
              <span class="error text-danger" for="fecha">{{ $errors->first('fecha') }}</span>
            @endif
            
            <div class="form-group" style="display:none;">>
              <label class="form-control-label" for="residente_id">{{ __('residente_id') }}</label>
              <input type="number" name="residente_id" id="residente_id" class="form-control"  value="{{$residente->id}}">
            </div>
            

            <div class="text-center">
              <a href="/residentes/{{$residente->id}}/historiales" class="btn btn-secondary mt-4">{{ __('Cancelar') }}</a>
              <button type="submit" class="btn btn-success mt-4">{{ __('Guardar') }}</button>
            </div>

          </div>
        </form>
      </div>
    </div>
  </div>
@endsection

@push('js')
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.extension.js"></script>
@endpush