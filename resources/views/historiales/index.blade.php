@extends('layouts.app')

@section('content')
  @include('layouts.headers.header')
  <!-- Tabla -->
  <div class="container-fluid mt--6">
    <div class="pb-2 pr-1 text-right"><a href="historiales/create" class="btn btn-sm btn-neutral">Ingresar Historial</a></div>
    <div class="row">
      <div class="col card mr-4 ml-4 mb-4">
        <div class="card-body">
          <h2 class="card-title">Tratamientos</h5>
          <p class="card-text">Cuidados y medicación asignada al residente para su recuperación adecuada.</p>
          <a href="@if (isset($historiales[0]) ) ../../tratamientos/{{ $historiales->first()->residente_id}} @endif" class="card-link">Ver</a>
          
        </div>
      </div>
      <div class="col card mr-4 ml-4 mb-4">
        <div class="card-body">
          <h2 class="card-title">Dieta</h5>
          <p class="card-text">Información sobre la alimentación que debe llevar, lo que debe o no comer.</p>
          <a href="@if (isset($historiales[0]) ) ../../dietas/{{$historiales->first()->residente_id}} @endif" class="card-link">Ver</a>
          <a href="../../dietas/{{$historiales->first()->id}}/create" class="card-link">Insertar</a>
        </div>
      </div>
    </div>
    <div class="row">
        <div class="col">
          <div class="card bg-default shadow">
            <div class="card-header bg-transparent border-0">
              <div class="row responsive">
                <div class="col-8">
                  <h3 class="text-white mb-0">
                  @if (isset($historiales[0]) )
                      {{  "Historial de ".$historiales[0]->residente->nombre.' '.$historiales[0]->residente->apellido}}
                  @else {{ "No tiene historial"}}
                  @endif
                  </h3>
                </div>
              </div>
            </div>
            <div class="table-responsive">
              <table class="table align-items-center table-dark table-flush">
                <thead class="thead-dark">
                  <tr>
                    <th scope="col" class="sort">id</th>
                    <th scope="col" class="sort">descripcion_enfermedad</th>
                    <th scope="col" class="sort">fecha</th>
                    <th scope="col"></th>
                  </tr>
                </thead>
                <tbody class="list">
                  @foreach ($historiales as $historial)
                    <tr>
                      <td>{{$historial->id}}</td>
                      <td>{{$historial->descripcion_enfermedad}}</td>
                      <td>{{$historial->fecha}}</td>
                      <td class="text-right">
                        <div class="dropdown">
                          <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-ellipsis-v"></i>
                          </a>
                          <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                            <a class="dropdown-item" href="historiales/{{$historial->id}}">mostrar</a>
                            <a class="dropdown-item" href="/historiales/{{$historial->id}}/edite">Editar</a>
                            <form action="{{route('historiales.destroye',$historial->id)}}" method="POST">
                              @csrf
                              @method('DELETE')
                              <button type="submit" class="dropdown-item">Eliminar</button>
                            </form>
                          </div>
                        </div>
                      </td>
                    </tr>
                @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
    </div>
  </div>
@endsection

@push('js')
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.extension.js"></script>
@endpush