@extends('layouts.app')

@section('content')
  @include('layouts.headers.header')
  <!-- Tabla -->
  <div class="container-fluid mt--6">
    <div class="row">
        <div class="col-xl">
                <div class="card card-profile shadow">
                    <div class="row justify-content-center">
                        <div class="col-lg-3 order-lg-2">
                            <div class="card-profile-image">
                                <a href="#">
                                    {{--@if($residente->imagen === null)--}}
                                      <img src="{{ asset('img') }}/residente/img.jpg" class="rounded-ciracle">
                                    {{--@else
                                      <img src="{{ asset('img') }}/residente/{$residente->imagen}}" class="rounded-circle">
                                    @endif--}}
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="card-header text-center border-0 pt-8 pt-md-4 pb-0 pb-md-4"></div>
                    <div class="card-body pt-0 pt-md-4">
                        <div class="row">
                            <div class="col">
                                <div class="card-profile-stats d-flex justify-content-center mt-md-5">
                                    <div>
                                        <span class="description">{{ __('Nombre') }}</span>
                                        <span class="heading">{{$residente[0]->nombre}}</span>
                                    </div>
                                    <div>
                                        <span class="description">{{ __('Apellido') }}</span>
                                        <span class="heading">{{$residente[0]->apellido}}</span>
                                    </div>
                                    <div>
                                        <span class="description">{{ __('Edad') }}</span>
                                        <span class="heading">{{$residente[0]->edad}}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
      </div>
      <div class="table-responsive">
        <table class="table align-items-center table-dark table-flush">
          <thead class="thead-dark">
            <tr>
              <th scope="col" class="sort">Descripcion</th>
              <th scope="col" class="sort"data-sort="name">Fecha</th>
              <th scope="col" class="sort">Hora</th>
              <th scope="col" class="sort">dosis</th>
              <th scope="col" class="sort">Empleado</th>
              <th scope="col" class="sort">Medicamento</th>
              <th scope="col"></th>
            </tr>
          </thead>
          <tbody class="list">
            @foreach ($tratamientos as $tratamiento)
              <tr>
                <td>{{$tratamiento->descripcion}}</td>
                <td>{{$tratamiento->fecha}}</td>
                <td>{{$tratamiento->hora}}</td>
                <td>{{$tratamiento->dosis}}</td>
                <td>{{$tratamiento->empleado->nombre}}</td>
                <td>{{$tratamiento->medicina->nombre}}</td>
                <td class="text-right">
                  <div class="dropdown">
                    <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="fas fa-ellipsis-v"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                      <a class="dropdown-item" href="">Editar</a>
                      <form id="{{$tratamiento->id}}" action="" method="POST">
                        @csrf
                        @method('DELETE')
                        <a class="dropdown-item" href="javascript:{}" onclick="document.getElementById('{{$tratamiento->id}}').submit(); return false;">Eliminar</a>
                      </form>
                    </div>
                  </div>
                </td>
              </tr>
          @endforeach
          </tbody>
        </table>
      </div>
    </div>
 
@endsection

@push('js')
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.extension.js"></script>
@endpush