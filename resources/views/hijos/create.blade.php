@extends('layouts.app', ['title' => __('Ingresar hijos')])

@section('content')
  @include('layouts.headers.header')
  <div class="container-fluid mt--7">
    <div class="card bg-secondary shadow">
      <div class="card-header bg-white border-0">
        <div class="row align-items-center">
          <h3 class="mb-0">{{ __('Ingresar encargado') }}</h3>
        </div>
      </div>
      <div class="card-body">
        <form method="post" action="/hijos" autocomplete="off">
          @csrf

          <h6 class="heading-small text-muted mb-4">{{ __('Información del encargado') }}</h6>
                                
          <div class="pl-lg-4">
            <div class="form-group">
              <label class="form-control-label" for="nombre">{{ __('Nombre') }}</label>
              <input type="text" name="nombre" id="nombre" class="form-control" value="{{ old('nombre') }}" required autofocus>
            </div>
            @if ($errors->has('nombre'))
              <span class="error text-danger" for="nombre">{{ $errors->first('nombre') }}</span>
            @endif
            <div class="form-group">
              <label class="form-control-label" for="apellido">{{ __('Apellido') }}</label>
              <input type="text" name="apellido" id="apellido" class="form-control" value="{{ old('apellido') }}" required>
            </div>
            @if ($errors->has('apellido'))
              <span class="error text-danger" for="apellido">{{ $errors->first('apellido') }}</span>
            @endif
            <div class="form-group">
              <label class="form-control-label" for="ci">{{ __('CI') }}</label>
              <input type="text" name="ci" id="ci" class="form-control" value="{{ old('ci') }}" required>
            </div>
            @if ($errors->has('ci'))
              <span class="error text-danger" for="ci">{{ $errors->first('ci') }}</span>
            @endif
            <div class="form-group">
              <label class="form-control-label" for="telefono">{{ __('Teléfono') }}</label>
              <input type="text" name="telefono" id="telefono" class="form-control" value="{{ old('telefono') }}" required>
              <input type="hidden" name="residente_id" value="{{$residente->id}}">
            </div>
            @if ($errors->has('telefono'))
              <span class="error text-danger" for="telefono">{{ $errors->first('telefono') }}</span>
            @endif
            <div class="text-center">
              <a href="/residentes/{{$residente->id}}" class="btn btn-secondary mt-4">{{ __('Saltar') }}</a>
              {{--<a href="/hijos/{{$residente->id}}" class="btn btn-secondary mt-4">{{ __('Agregar') }}</a>--}}
              <button type="submit" class="btn btn-success mt-4">{{ __('Guardar') }}</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
@endsection

@push('js')
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.extension.js"></script>
@endpush