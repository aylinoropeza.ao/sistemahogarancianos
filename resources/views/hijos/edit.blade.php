@extends('layouts.app', ['title' => __('Editar residente')])

@section('content')
  @include('layouts.headers.header')
  <div class="container-fluid mt--7">
    <div class="card bg-secondary shadow">
      <div class="card-header bg-white border-0">
        <div class="row align-items-center">
          <h3 class="mb-0">{{ __('Editar encargado') }}</h3>
        </div>
      </div>
      <div class="card-body">
        <form method="post" action="/hijos/{{ $hijo->id }}" autocomplete="off">
          @csrf
          @method('put')
          <h6 class="heading-small text-muted mb-4">{{ __('Información del encargado del residente') }}</h6>
                                
          <div class="pl-lg-4">
            <div class="form-group">
              <label class="form-control-label" for="nombre">{{ __('Nombre') }}</label>
              <input type="text" name="nombre" id="nombre" class="form-control" value="{{$hijo->nombre}}" required autofocus>
            </div>
            <div class="form-group">
              <label class="form-control-label" for="apellido">{{ __('Apellido') }}</label>
              <input type="text" name="apellido" id="apellido" class="form-control" value="{{$hijo->apellido}}" required>
            </div>
            <div class="form-group">
              <label class="form-control-label" for="ci">{{ __('CI') }}</label>
              <input type="text" name="ci" id="ci" class="form-control" value="{{$hijo->ci}}" required>
            </div>
            <div class="form-group">
              <label class="form-control-label" for="telefono">{{ __('Teléfono') }}</label>
              <input type="text" name="telefono" id="telefono" class="form-control" value="{{$hijo->telefono}}" required>
            </div>
            @if ($errors->any())
                <div class="alert alert-danger">
                  <ul>
                    @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                    @endforeach
                  </ul>
                </div>
            @endif
            <div class="text-center">
              <a href="/residentes/{{$hijo->residente_id}}" class="btn btn-secondary mt-4">{{ __('Cancelar') }}</a>
              <button type="submit" class="btn btn-success mt-4">{{ __('Guardar') }}</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
@endsection

@push('js')
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.extension.js"></script>
@endpush