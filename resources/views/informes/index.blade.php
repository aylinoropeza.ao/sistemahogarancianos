@extends('layouts.app')

@section('content')
    @include('layouts.headers.header')
    <!-- Tabla -->
    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col-xl-4">
                <div class="card card-profile shadow">
                    <div class="card-body pt-0 pt-md-4">
                        <div class="row align-center">
                            <a href="/informes/pdfIngresos"
                                class="col btn btn-sm btn-info ml-4 mr-4">{{ __(' Informe pdf Ingresos ') }}</a>
                        </div>
                        <p></p>
                        <div class="row align-center">
                            <a href="/informes/pdfEgresos"
                                class="col btn btn-sm btn-info ml-4 mr-4">{{ __(' Informe pdf Egresos ') }}</a>
                        </div>
                        <p></p>
                        <div class="row align-center">
                            <a href="/informes/pdfTotal"
                                class="col btn btn-sm btn-info ml-4 mr-4">{{ __('  Informe pdf todo ') }}</a>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-xl-4">
                <div class="card card-profile shadow">
                    <div class="card-body pt-0 pt-md-4">

                        <form method="post" action="informes/pdfIngresosFecha" autocomplete="off">
                            @csrf

                            <h6 class="heading-small text-muted mb-4">{{ __('Informe por fechas') }}</h6>

                            <div class="pl-lg-4">
                                <div class="form-group">
                                    <label class="form-control-label" for="fecha_inicio">{{ __('fecha_inicio') }}</label>
                                    <input type="date" name="fecha_inicio" id="fecha_inicio" class="form-control"
                                        value="{{ old('fecha inicio') }}" required>
                                </div>
                                @if ($errors->has('fecha_inicio'))
                                    <span class="error text-danger"
                                        for="fecha_inicio">{{ $errors->first('fecha_inicio') }}</span>
                                @endif

                                <div class="form-group">
                                    <label class="form-control-label" for="fecha_fin">{{ __('fecha fin') }}</label>
                                    <input type="date" name="fecha_fin" id="fecha_fin" class="form-control"
                                        value="{{ old('fecha_fin') }}" required>
                                </div>
                                @if ($errors->has('fecha_fin'))
                                    <span class="error text-danger"
                                        for="fecha_fin">{{ $errors->first('fecha_fin') }}</span>
                                @endif




                                <div class="row align-center">
                                    <button type="submit"
                                        class="col btn btn-sm btn-info ml-4 mr-4">{{ __(' Informe pdf entre fechas') }}</button>
                                </div>
                                <p></p>


                            </div>
                        </form>

                    </div>
                </div>

            </div>
            <div class="col-xl-4">
                <div class="card card-profile shadow">
                    <div class="card-body pt-0 pt-md-4">

                        <form method="post" action="informes/pdfGastosResidente" autocomplete="off">
                            @csrf

                            <h6 class="heading-small text-muted mb-4">{{ __('Informe por Gastos Residente') }}</h6>

                            <div class="form-group">
                                <label class="form-control-label" for="residente">{{ __('Residentes') }}</label>
                                <select class="form-control select2" id="residente" name="residente">
                                  <option>-- Seleccionar --</option>
                                  @foreach($residentes as $residente)
                                    <option value="{{$residente->id}}" @if($residente == old('residente')) selected @endif>{{$residente->nombre}} ({{$residente->ci}})</option>
                                  @endforeach
                                </select>
                              </div>

                                <div class="row align-center">
                                    <button type="submit"
                                        class="col btn btn-sm btn-info ml-4 mr-4">{{ __(' Informe pdf') }}</button>
                                </div>
                                <p></p>


                            </div>
                        </form>

                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@push('js')
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.extension.js"></script>
@endpush
