@extends('layouts.app', ['title' => __('Nuevo registro')])

@section('content')
  @include('layouts.headers.header')
  <div class="container-fluid mt--7">
    <div class="card bg-secondary shadow">
      <div class="card-header bg-white border-0">
        <div class="row align-items-center">
          <h3 class="mb-0">{{ __('Nuevo registro') }}</h3>
        </div>
      </div>
      <div class="card-body">
        <form method="post" action="/libroGastos" autocomplete="off">
          @csrf
          <h6 class="heading-small text-muted mb-4">{{ __('Ingresar datos') }}</h6>
                                
          <div class="pl-lg-4">
          <div class="form-group">
              <label class="form-control-label" for="descripcion">{{ __('Descripción') }}</label>
              <input type="text" name="descripcion" id="descripcion" class="form-control" value="{{ old('descripcion') }}" required autofocus>
            </div>
            @if ($errors->has('descripcion'))
              <span class="error text-danger" for="descripcion">{{ $errors->first('descripcion') }}</span>
            @endif
            <div class="form-group row">
              <div class="col">
                <label class="form-control-label" for="precio">{{ __('Precio') }}</label>
                <input type="number" name="precio" id="precio" class="form-control" value="{{ old('precio') }}" required>
              </div>
              <div class="col">
                <label class="form-control-label" for="fecha">{{ __('Fecha') }}</label>
                <input type="date" name="fecha" id="fecha" class="form-control" value="{{ old('fecha') }}" required>
                @if ($errors->has('fecha'))
                  <span class="error text-danger" for="fecha">{{ $errors->first('fecha') }}</span>
                @endif
              </div>
            </div>
            <div class="form-group">
              <label class="form-control-label">{{ __('Estado') }}</label>
              <div class="radio">
                <label><input type="radio" name="estado_pagado" value="1" @if(old('estado') == 1) checked @endif>Pagado</label>
              </div>
              <div class="radio">
                <label><input type="radio" name="estado_pagado" value="0" @if(old('estado') == 0) checked @endif>Por pagar</label>
              </div>
            </div>
            <div class="form-group">
              <label class="form-control-label" for="residente_id">{{ __('Residente') }}</label>
              <select class="form-control select2" id="residente_id" name="residente_id">
                <option>-- Seleccionar --</option>
                @foreach($residentes as $residente)
                  <option value="{{$residente->id}}" @if($residente == old('residente_id')) selected @endif>{{$residente->nombre}} {{$residente->apellido}}</option>
                @endforeach
              </select>
            </div>
            @if ($errors->has('residente_id'))
              <span class="error text-danger" for="residente_id">{{ $errors->first('residente_id') }}</span>
            @endif
            <div class="text-center">
              <a href="/libroGastos" class="btn btn-secondary mt-4">{{ __('Cancelar') }}</a>
              <button type="submit" class="btn btn-success mt-4">{{ __('Guardar') }}</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
@endsection

@push('js')
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.extension.js"></script>
    <!--  Select2 -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script> 

    <script>
      $(document).ready(function() {
            // Select2 Multiple
            $('.select2').select2({
                placeholder: "Select",
                allowClear: true
            });

      });
    </script> 

@endpush