@extends('layouts.app', ['title' => __('Editar registro')])

@section('content')
  @include('layouts.headers.header')
  <div class="container-fluid mt--7">
    <div class="card bg-secondary shadow">
      <div class="card-header bg-white border-0">
        <div class="row align-items-center">
          <h3 class="mb-0">{{ __('Editar registro') }}</h3>
        </div>
      </div>
      <div class="card-body">
        <form method="post" action="/libroGastos/{{$libroGasto->id}}" autocomplete="off">
          @csrf
          @method('put')
          <h6 class="heading-small text-muted mb-4">{{ __('Ingresar datos') }}</h6>
                                
          <div class="pl-lg-4">
            <div class="form-group">
              <label class="form-control-label" for="descripcion">{{ __('Descripción') }}</label>
              <input type="text" name="descripcion" id="descripcion" class="form-control" value="{{ $libroGasto->descripcion }}" required autofocus>
            </div>
            <div class="form-group row">
              <div class="col">
                <label class="form-control-label" for="precio">{{ __('Precio') }}</label>
                <input type="number" name="precio" id="precio" class="form-control" value="{{ $libroGasto->precio }}" required>
              </div>
              <div class="col">
                <label class="form-control-label" for="fecha">{{ __('Fecha') }}</label>
                <input type="date" name="fecha" id="fecha" class="form-control" value="{{ $libroGasto->fecha }}" required>
              </div>
            </div>
            <div class="form-group">
              <label class="form-control-label">{{ __('Estado') }}</label>
              <div class="radio">
                <label><input type="radio" name="estado_pagado" value="1" @if($libroGasto->estado_pagado == 1) checked @endif>Pagado</label>
              </div>
              <div class="radio">
                <label><input type="radio" name="estado_pagado" value="0" @if($libroGasto->estado_pagado == 0) checked @endif>Por pagar</label>
              </div>
            </div>
            <div class="form-group">
              <label class="form-control-label" for="residente_id">{{ __('Residente') }}</label>
              <select class="form-control select2" id="residente_id" name="residente_id">
                @foreach($residentes as $residente)
                  <option value="{{$residente->id}}" @if($libroGasto->residente_id == $residente->id) selected @endif>{{$residente->nombre}} {{$residente->apellido}}</option>
                @endforeach
              </select>
            </div>
            <div class="text-center">
              <a href="/libroGastos" class="btn btn-secondary mt-4">{{ __('Cancelar') }}</a>
              <button type="submit" class="btn btn-success mt-4">{{ __('Guardar') }}</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
@endsection

@push('js')
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.extension.js"></script>
@endpush