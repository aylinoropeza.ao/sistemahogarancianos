@extends('layouts.app')

@section('content')
  @include('layouts.headers.header')
  <!-- Tabla -->
  <div class="container-fluid mt--6">
    <div class="pb-2 pr-1 text-right"><a href="libroGastos/create" class="btn btn-sm btn-neutral">Adicionar al libro de gastos</a></div>
    <div class="row">
        <div class="col">
          <div class="card bg-default shadow">
            <div class="card-header bg-transparent border-0">
              <div class="row responsive">
                <div class="col-8"><h3 class="text-white mb-0">Libro de gastos</h3></div>
              </div>
            </div>
            <div class="table-responsive">
              <table class="table align-items-center table-dark table-flush">
                <thead class="thead-dark">
                  <tr>
                    <th scope="col" class="sort">Id</th>
                    <th scope="col" class="sort" data-sort="name">Residente</th>
                    <th scope="col" class="sort">Descripción</th>
                    <th scope="col" class="sort">Precio</th>
                    <th scope="col" class="sort">Estado</th>
                    <th scope="col" class="sort">Fecha</th>
                    <th scope="col"></th>
                  </tr>
                </thead>
                <tbody class="list">
                  @foreach ($libro_gastos as $libro_gasto)
                    <tr>
                      <td>{{ $libro_gasto->id }}</td>
                      <td>{{ $libro_gasto->residente->nombre }} {{ $libro_gasto->residente->apellido }}</td>
                      <td>{{ $libro_gasto->descripcion }}</td>
                      <td>{{ $libro_gasto->precio }}</td>
                      <td>@if($libro_gasto->estado_pagado == 1) Pagado @else Por pagar @endif</td>
                      <td>{{ $libro_gasto->fecha }}</td>
                      <td class="text-right">
                        <div class="dropdown">
                          <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-ellipsis-v"></i>
                          </a>
                          <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                            <a class="dropdown-item" href="{{route('libroGastos.edit',$libro_gasto->id)}}">Editar</a>
                            <form id="{{$libro_gasto->id}}" action="{{route('libroGastos.destroy', $libro_gasto->id)}}" method="POST">
                              @csrf
                              @method('DELETE')
                              <a class="dropdown-item" href="javascript:{}" onclick="document.getElementById('{{$libro_gasto->id}}').submit(); return false;">Eliminar</a>
                            </form>
                          </div>
                        </div>
                      </td>
                    </tr>
                @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
    </div>
  </div><br>
  <div class="text-center">{{ $libro_gastos->links() }}</div><br>
@endsection

@push('js')
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.extension.js"></script>
@endpush