@extends('layouts.app', ['title' => __('Libro de ingresos')])

@section('content')
  @include('layouts.headers.header')
  <div class="container-fluid mt--7">
    <div class="card bg-secondary shadow">
      <div class="card-header bg-white border-0">
        <div class="row align-items-center">
          <h3 class="mb-0">{{ __('Adicionar al libro Ingreso') }}</h3>
        </div>
      </div>
      <div class="card-body">
        <form method="post" action="/libroIngresos" autocomplete="off">
          @csrf

          <h6 class="heading-small text-muted mb-4">{{ __('Ingresar datos') }}</h6>
                                
          <div class="pl-lg-4">
            <div class="form-group">
              <label class="form-control-label" for="fecha">Fecha</label>
                  <input type="date" id="fecha" name="fecha" class="form-control" placeholder="fecha" required>
            </div>
            <div class="form-group">
              <label class="form-control-label" for="concepto">{{ __('Concepto') }}</label>
              <input type="text" name="concepto" id="concepto" class="form-control" required>
            </div>
            <div class="form-group">
              <label class="form-control-label" for="comentario">{{ __('Comentario') }}</label>
              <input type="text" name="comentario" id="comentario" class="form-control" required>
            </div>
            <div class="form-group">
              <label class="form-control-label" for="saldo">{{ __('Saldo') }}</label>
              <input type="number" name="saldo" id="saldo" class="form-control" required>
            </div>
            <div class="form-group">
              <label class="form-control-label" for="tipo">{{ __('Tipo de Ingresos') }}</label>
              <select class="form-control" id="tipo" name="tipo">
               
                @foreach($tipos as $tipo)
                  <option value="{{$tipo}}">{{ $tipo }}</option>
                @endforeach
              </select>
            </div>

            <div class="text-center">
              <a href="/libroIngresos" class="btn btn-secondary mt-4">{{ __('Cancelar') }}</a>
              <button type="submit" class="btn btn-success mt-4">{{ __('Guardar') }}</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
@endsection

@push('js')
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.extension.js"></script>
@endpush