@extends('layouts.app', ['title' => __('Ingresar residente')])

@section('content')
  @include('layouts.headers.header')
  <div class="container-fluid mt--7">
    <div class="card bg-secondary shadow">
      <div class="card-header bg-white border-0">
        <div class="row align-items-center">
          <h3 class="mb-0">{{ __('Ingresar residente') }}</h3>
        </div>
      </div>
      <div class="card-body">
        <form method="post" action="/residentes" enctype="multipart/form-data" autocomplete="off">
          @csrf

          <h6 class="heading-small text-muted mb-4">{{ __('Información del residente') }}</h6>
                                
          <div class="pl-lg-4">
            <div class="row">
              <div class="form-group col">
                <label class="form-control-label" for="nombre">{{ __('Nombre') }}</label>
                <input type="text" name="nombre" id="nombre" class="form-control" value="{{ old('nombre') }}" required autofocus>
              </div>
              <div class="form-group col">
                <label class="form-control-label" for="apellido">{{ __('Apellido') }}</label>
                <input type="text" name="apellido" id="apellido" class="form-control" value="{{ old('apellido') }}" required>
              </div>
            </div>
            <div class="row">
              <div class="col">
              @if ($errors->has('nombre'))
                <span class="error text-danger" for="nombre">{{ $errors->first('nombre') }}</span>
              @endif
              </div>
              <div class="col">
              @if ($errors->has('apellido'))
                <span class="error text-danger" for="apellido">{{ $errors->first('apellido') }}</span>
              @endif
              </div>
            </div>
            <div class="row">
              <div class="form-group col">
                <label class="form-control-label" for="ci">{{ __('CI') }}</label>
                <input type="text" name="ci" id="ci" class="form-control" value="{{ old('ci') }}" required>
              </div>
              <div class="form-group col">
                <label class="form-control-label" for="fecha_nacimiento">{{ __('Fecha de nacimiento') }}</label>
                <input type="date" name="fecha_nacimiento" id="fecha_nacimiento" class="form-control" value="{{ old('fecha_nacimiento') }}" required>
              </div>
            </div>
            <div class="row">
              <div class="col">
              @if ($errors->has('ci'))
                <span class="error text-danger" for="ci">{{ $errors->first('ci') }}</span>
              @endif
              </div>
              <div class="col">
              @if ($errors->has('fecha_nacimiento'))
                <span class="error text-danger" for="fecha_nacimiento">{{ $errors->first('fecha_nacimiento') }}</span>
              @endif
              </div>
            </div>
            <div class="row">
              <div class="form-group col">
                <label class="form-control-label" for="num_habitacion">{{ __('Número de habitación') }}</label>
                <input type="number" name="num_habitacion" id="num_habitacion" class="form-control" value="{{ old('num_habitacion') }}" required>
              </div>
              <div class="col">
                <div class="form-group">
                  <label class="form-control-label" for="imagen">{{ __('Imagen') }}</label>
                  <input type="file" name="imagen" id="imagen" class="form-control">
                </div>
              </div>
            </div>
            <div class="text-center">
              <a href="/residentes" class="btn btn-secondary mt-4">{{ __('Cancelar') }}</a>
              <button type="submit" class="btn btn-success mt-4">{{ __('Guardar') }}</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
@endsection

@push('js')
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.extension.js"></script>
@endpush