@extends('layouts.app')

@section('content')
  @include('layouts.headers.header')
  <!-- Tabla -->
  <div class="container-fluid mt--6">
    <div class="pb-2 pr-1 text-right"><a href="residentes/create" class="btn btn-sm btn-neutral">Ingresar residente</a></div>
    <div class="row">
        <div class="col">
          <div class="card bg-default shadow">
            <div class="card-header bg-transparent border-0">
              <div class="row responsive">
                <div class="col-8"><h3 class="text-white mb-0">Lista de Residentes</h3></div>
              </div>
            </div>
            <div class="table-responsive">
              <table class="table align-items-center table-dark table-flush">
                <thead class="thead-dark">
                  <tr>
                    <th scope="col" class="sort">Id</th>
                    <th scope="col" class="sort" data-sort="name">Nombre</th>
                    <th scope="col" class="sort">Apellido</th>
                    <th scope="col" class="sort">CI</th>
                    <th scope="col" class="sort">Número de encargados</th>
                    <th scope="col" class="sort">Fecha de nacimiento</th>
                    <th scope="col" class="sort">Habitación</th>
                    <th scope="col"></th>
                  </tr>
                </thead>
                <tbody class="list">
                  @foreach ($residentes as $residente)
                    <tr>
                      <td>{{$residente->id}}</td>
                      <td>{{$residente->nombre}}</td>
                      <td>{{$residente->apellido}}</td>
                      <td>{{$residente->ci}}</td>
                      <td>{{$residente->hijos_count}} Encargado(s)</td>
                      <td >{{$residente->fecha_nacimiento}}</td>
                      <td>{{$residente->num_habitacion}}</td>
                      <td class="text-right">
                        <div class="dropdown">
                          <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-ellipsis-v"></i>
                          </a>
                          <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                            <a class="dropdown-item" href="/residentes/{{$residente->id}}">Más información</a>
                            <a class="dropdown-item" href="/residentes/{{$residente->id}}/pdf">PDF</a>
                            <a class="dropdown-item" href="/residentes/{{$residente->id}}/historiales">{{ __('Historial') }}</a>
                            <form id="{{$residente->id}}" action="{{route('residentes.destroy', $residente->id)}}" method="POST">
                              @csrf
                              @method('DELETE')
                              <a class="dropdown-item" href="javascript:{}" onclick="document.getElementById('{{$residente->id}}').submit(); return false;">Eliminar</a>
                            </form>
                          </div>
                        </div>
                      </td>
                    </tr>
                @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
    </div>
  </div>
@endsection

@push('js')
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.extension.js"></script>
@endpush