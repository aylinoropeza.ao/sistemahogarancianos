@extends('layouts.app')

@section('content')
  @include('layouts.headers.header')
  <!-- Tabla -->
  <div class="container-fluid mt--6  text-center">
    <div class="row">
        <div class="@if($hijos->first() != null) col-xl-5 @else col-xl-8 offset-md-2 @endif">
                <div class="card card-profile shadow">
                    <div class="row justify-content-center">
                        <div class="col-lg-3 order-lg-2">
                            <div class="card-profile-image">
                                <a href="#">
                                    @if($residente->imagen === null)
                                      <img src="{{ asset('img') }}/residente/img.jpg" class="rounded-circle">
                                    @else
                                      <img src="{{ asset('img') }}/residente/{{$residente->imagen}}" class="rounded-circle">
                                    @endif
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="card-header text-center border-0 pt-8 pt-md-4 pb-0 pb-md-4">
                        <div class="d-flex justify-content-between">
                            <a href="/hijos/{{$residente->id}}" class="btn btn-sm btn-info mr-4">{{ __('+ Encargado') }}</a>
                            <a href="/residentes/{{$residente->id}}/edit" class="btn btn-sm btn-default float-right">{{ __('Editar') }}</a>
                        </div>
                    </div>
                    <div class="card-body pt-0 pt-md-4">
                        <div class="row">
                            <div class="col">
                                <div class="card-profile-stats d-flex justify-content-center mt-md-5">
                                    <div>
                                        <span class="description">{{ __('Nombre') }}</span>
                                        <span class="heading">{{$residente->nombre}}</span>
                                    </div>
                                    <div>
                                        <span class="description">{{ __('Apellido') }}</span>
                                        <span class="heading">{{$residente->apellido}}</span>
                                    </div>
                                    <div>
                                        <span class="description">{{ __('CI') }}</span>
                                        <span class="heading">{{$residente->ci}}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row align-center">
                          <a href="/libroGastos/{{$residente->id}}" class="col btn btn-sm btn-info ml-4 mr-4">{{ __(' Libro de gastos personales ') }}</a>
                          <a href="/residentes/{{$residente->id}}/historiales" class="col btn btn-sm btn-info ml-4 mr-4">{{ __(' Historial ') }}</a>
                        </div>
                    </div>
                </div>
        </div>
        @if($hijos->first() != null)
        <div class="col-xl-7">
          <div class="card bg-default shadow">
            <div class="card-header bg-transparent border-0">
              <div class="row responsive">
                <div class="col-8"><h3 class="text-white mb-0">Hijos del residente</h3></div>
              </div>
            </div>
            <div class="table-responsive">
              <table class="table align-items-center table-dark table-flush">
                <thead class="thead-dark">
                  <tr>
                    <th scope="col" class="sort">Id</th>
                    <th scope="col" class="sort" data-sort="name">Nombre</th>
                    <th scope="col" class="sort">Apellido</th>
                    <th scope="col" class="sort">CI</th>
                    <th scope="col" class="sort">Teléfono</th>
                    <th scope="col"></th>
                  </tr>
                </thead>
                <tbody class="list">
                  @foreach ($hijos as $hijo)
                    <tr>
                      <td>{{$hijo->id}}</td>
                      <td>{{$hijo->nombre}}</td>
                      <td>{{$hijo->apellido}}</td>
                      <td>{{$hijo->ci}}</td>
                      <td>{{$hijo->telefono}}</td>
                      <td>
                        <div class="dropdown">
                          <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-ellipsis-v"></i>
                          </a>
                          <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                            <a class="dropdown-item" href="/hijos/{{$hijo->id}}/edit">{{ __('Editar') }}</a>
                            <form id="{{$hijo->id}}" action="{{route('hijos.destroy', $hijo->id)}}" method="POST">
                              @csrf
                              @method('DELETE')
                              <a class="dropdown-item" href="javascript:{}" onclick="document.getElementById('{{$hijo->id}}').submit(); return false;">Eliminar</a>
                            </form>
                          </div>
                        </div>
                      </td>
                    </tr>
                @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
        @endif
    </div>
  </div>
@endsection

@push('js')
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.extension.js"></script>
@endpush