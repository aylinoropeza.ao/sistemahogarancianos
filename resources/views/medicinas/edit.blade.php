@extends('layouts.app', ['title' => __('Editar medicina')])

@section('content')
  @include('layouts.headers.header')
  <div class="container-fluid mt--7">
    <div class="card bg-secondary shadow">
      <div class="card-header bg-white border-0">
        <div class="row align-items-center">
          <h3 class="mb-0">{{ __('Editar medicamento') }}</h3>
        </div>
      </div>
      <div class="card-body">
        <form method="post" action="/medicinas/{{$medicina->id}}" autocomplete="off">
          @csrf
          @method('put')
          <h6 class="heading-small text-muted mb-4">{{ __('Información del medicamento') }}</h6>
                                
          <div class="pl-lg-4">
            <div class="form-group">
              <label class="form-control-label" for="nombre">{{ __('Nombre') }}</label>
              <input type="text" name="nombre" id="nombre" class="form-control" value="{{$medicina->nombre}}" required autofocus>
            </div>
            @if ($errors->has('nombre'))
              <span class="error text-danger" for="nombre">{{ $errors->first('nombre') }}</span>
            @endif
            <div class="form-group">
              <label class="form-control-label" for="descripcion">{{ __('Descripción') }}</label>
              <input type="text" name="descripcion" id="descripcion" class="form-control" value="{{$medicina->descripcion}}" required>
            </div>
            @if ($errors->has('descripcion'))
              <span class="error text-danger" for="descripcion">{{ $errors->first('descripcion') }}</span>
            @endif
            <div class="form-group">
              <label class="form-control-label" for="stock">{{ __('Stock') }}</label>
              <input type="number" name="stock" id="stock" class="form-control" value="{{$medicina->stock}}" required>
            </div>
            @if ($errors->has('stock'))
              <span class="error text-danger" for="stock">{{ $errors->first('stock') }}</span>
            @endif
            <div class="form-group">
              <label class="form-control-label" for="precio">{{ __('Precio') }}</label>
              <input type="text" name="precio" id="precio" class="form-control" value="{{$medicina->precio}}" required>
            </div>
            @if ($errors->has('precio'))
              <span class="error text-danger" for="precio">{{ $errors->first('precio') }}</span>
            @endif
            <div class="form-group">
              <label class="form-control-label" for="via">{{ __('Vía') }}</label>
              <select class="form-control" id="via" name="via">
                @foreach($vias as $via)
                  <option value="{{$via}}" @if($via == $medicina->via) selected @endif>{{ $via }}</option>
                @endforeach
              </select>
            </div>
            @if ($errors->has('via'))
              <span class="error text-danger" for="via">{{ $errors->first('via') }}</span>
            @endif
            <div class="text-center">
              <a href="/medicinas" class="btn btn-secondary mt-4">{{ __('Cancelar') }}</a>
              <button type="submit" class="btn btn-success mt-4">{{ __('Guardar') }}</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
@endsection

@push('js')
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.extension.js"></script>
@endpush