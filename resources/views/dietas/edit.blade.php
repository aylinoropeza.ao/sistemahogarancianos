@extends('layouts.app', ['title' => __('Editar dieta')])

@section('content')
  @include('layouts.headers.header')
  <div class="container-fluid mt--7">
    <div class="card bg-secondary shadow">
      <div class="card-header bg-white border-0">
        <div class="row align-items-center">
          <h3 class="mb-0">{{ __('Editar la dieta') }}</h3>
        </div>
      </div>
      <div class="card-body">
        <form method="post" action="/dietas/{{$residente->id}}" autocomplete="off">
          @csrf
          @method('put')
          <h6 class="heading-small text-muted mb-4">{{ __('Editar datos') }}</h6>
          <div class="pl-lg-4">
          <div class="form-group">
              <label class="form-control-label" for="alimentos_prohibidos">{{ __('Alimentos prohibidos') }}</label>
              <textarea class="form-control" name="alimentos_prohibidos" id="alimentos_prohibidos" rows="4" required autofocus>{{ $dieta->alimentos_prohibidos }}</textarea>
            </div>
            @if ($errors->has('alimentos_prohibidos'))
              <span class="error text-danger" for="alimentos_prohibidos">{{ $errors->first('alimentos_prohibidos') }}</span>
            @endif
            <div class="form-group">
              <label class="form-control-label" for="alimentos_beneficioso">{{ __('Alimentos recomendados') }}</label>
              <textarea class="form-control" name="alimentos_beneficioso" id="alimentos_beneficioso" rows="4" required>{{ $dieta->alimentos_beneficioso }}</textarea>
              <input type="hidden" name="historial_id" value="{{$dieta->historial_id}}">
            </div>
            @if ($errors->has('alimentos_beneficioso'))
              <span class="error text-danger" for="alimentos_beneficioso">{{ $errors->first('alimentos_beneficioso') }}</span>
            @endif
            <div class="text-center">
              <a href="/dietas/{{$residente->id}}" class="btn btn-secondary mt-4">{{ __('Cancelar') }}</a>
              <button type="submit" class="btn btn-success mt-4">{{ __('Guardar') }}</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
@endsection

@push('js')
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.extension.js"></script>
@endpush