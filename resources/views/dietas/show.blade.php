@extends('layouts.app')

@section('content')
  @include('layouts.headers.header')
  <!-- Tabla -->
  <div class="container-fluid mt--6">
    <div class="row">
        <div class="col-xl">
                <div class="card card-profile shadow">
                    <div class="row justify-content-center">
                        <div class="col-lg-3 order-lg-2">
                            <div class="card-profile-image">
                                <a href="#">
                                  {{--dd($residente)--}}
                                    @if($residente->first()->imagen === null)
                                      <img src="{{ asset('img') }}/residente/img.jpg" class="rounded-circle">
                                    @else
                                      <img src="{{ asset('img') }}/residente/{{$residente->imagen}}" class="rounded-circle">
                                    @endif
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="card-header text-center border-0 pt-8 pt-md-4 pb-0 pb-md-4">
                        <div class="d-flex justify-content-between">
                            <a href="/residentes/{{$residente->id}}/historiales" class="btn btn-sm btn-info mr-4">{{ __('Volver a historial') }}</a>
                            @if (isset($dieta) )
                            <div class="row mr-4">
                            <a href="@if (isset($dieta) ) /dietas/{{$dieta->id}}/edit @endif" class="btn btn-sm btn-info ">{{ __('Editar') }}</a>
                            <form id="{{$dieta->id}}" action="{{route('dietas.destroy', $dieta->id)}}" method="POST">
                              @csrf
                              @method('DELETE')
                              <a class="btn btn-sm btn-default float-right" href="javascript:{}" onclick="document.getElementById('{{$dieta->id}}').submit(); return false;">Eliminar</a>
                            </form>
                            @endif
                            </div>
                        </div>
                    </div>
                    
                    <div class="card-body pt-0 pt-md-4">
                        <div class="row">
                            <div class="col">
                                <div class="card-profile-stats d-flex justify-content-center mt-md-5">
                                    <div>
                                        <span class="description">{{ __('Nombre') }}</span>
                                        <span class="heading">{{$residente->nombre}}</span>
                                    </div>
                                    <div>
                                        <span class="description">{{ __('Apellido') }}</span>
                                        <span class="heading">{{$residente->apellido}}</span>
                                    </div>
                                    <div>
                                        <span class="description">{{ __('Edad') }}</span>
                                        <span class="heading">{{$edad}}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{--dd($residente[0], $tratamiento, $medicinas[0])--}}
                        @if (isset($dieta) )

                        <h1 class="text-center">Dieta</h2><br>
                        <div class="row mx-5">
                          <div class="col text-center">
                            <h3>Alimentos prohibidos</h3>
                            <p>{{$dieta->alimentos_prohibidos}}</p>
                          </div>
                          <div class="col text-center">
                            <h3>Alimentos recomendados</h3>
                            <p>{{$dieta->alimentos_beneficioso}}</p>
                          </div>
                        </div>
                    </div>
                </div>
                @else {{ "No tiene dieta asignada"}}
              @endif
        </div>
    </div>
  </div>
@endsection

@push('js')
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.extension.js"></script>
@endpush