<?php

namespace Database\Seeders;

use App\Models\Residente;
use Illuminate\Database\Seeder;

class ResidenteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Residente::factory(10)->create();
    }
}
