<?php

namespace Database\Seeders;

use App\Models\Medicina;
use Illuminate\Database\Seeder;

class MedicinaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        Medicina::factory(25)->create();
    }
}
