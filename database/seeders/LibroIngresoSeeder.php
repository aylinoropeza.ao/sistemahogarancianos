<?php

namespace Database\Seeders;

use App\Models\LibroIngreso;
use Illuminate\Database\Seeder;

class LibroIngresoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        LibroIngreso::factory(100)->create();
    }
}
