<?php

namespace Database\Seeders;

use App\Models\SignosVitale;
use Illuminate\Database\Seeder;

class SignosVitaleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SignosVitale::factory(200)->create();
    }
}
