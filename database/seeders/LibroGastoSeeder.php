<?php

namespace Database\Seeders;

use App\Models\LibroGasto;
use Illuminate\Database\Seeder;

class LibroGastoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        LibroGasto::factory(50)->create();
    }
}
