<?php
namespace Database\Seeders;

use App\Models\Empleado;
use App\Models\Historiale;
use App\Models\Medicacione;
use App\Models\Medicina;
use App\Models\Residente;
use App\Models\SignosVitale;
use App\Models\Tratamiento;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        /*
        $medicamento = Medicina::factory()->hasMedicamentos(5)->create();
        $empleado = Empleado::factory()->hasEmpleados(5)->create();
        Medicacione::factory(5)->for($medicamento)->for($empleado)->create();
        */
        $this->call([UsersTableSeeder::class]);
        //Medicina::factory(5)->create();
       // Empleado::factory(5)->create();
       // Medicacione::factory(5)->create();
        
        $this->call([ResidenteSeeder::class]);
        $this->call([HijoSeeder::class]);
        $this->call([MedicinaSeeder::class]);
        $this->call([EmpleadoSeeder::class]);
        $this->call([HistorialeSeeder::class]);
        
        $this->call([TratamientoSeeder::class]);
        
        $this->call([DietaSeeder::class]);
        //$this->call([HistorialeSeeder::class]);

        $this->call([SignosVitaleSeeder::class]);
        $this->call([LibroGastoSeeder::class]);

        $this->call([LibroEgresoSeeder::class]);
        $this->call([LibroIngresoSeeder::class]);
    }
}
