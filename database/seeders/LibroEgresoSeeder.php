<?php

namespace Database\Seeders;

use App\Models\LibroEgreso;
use Illuminate\Database\Seeder;

class LibroEgresoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        LibroEgreso::factory(100)->create();
    }
}
