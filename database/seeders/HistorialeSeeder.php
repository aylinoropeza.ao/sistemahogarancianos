<?php

namespace Database\Seeders;

use App\Models\Historiale;
use Illuminate\Database\Seeder;

class HistorialeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Historiale::factory(20)->create();
    }
}
