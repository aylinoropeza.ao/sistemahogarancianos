<?php

namespace Database\Seeders;

use App\Models\Hijo;
use Illuminate\Database\Seeder;

class HijoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Hijo::factory(10)->create();
    }
}
