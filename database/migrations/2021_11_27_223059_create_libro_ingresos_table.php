<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLibroIngresosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('libro_ingresos', function (Blueprint $table) {

            $table->id();
            $table->date('fecha');
            $table->string('concepto', 30);
            $table->string('comentario')->nullable();
            $table->float('saldo', 12, 2);
            $table->enum('tipo', ['normal', 'donacion']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('libro_ingresos');
    }
}
