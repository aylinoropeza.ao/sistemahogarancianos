<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTratamientosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tratamientos', function (Blueprint $table) {
            $table->id();
            $table->string('descripcion');
            $table->time('hora');
            $table->date('fecha');
            $table->string('dosis');

            $table->unsignedBigInteger('historial_id');
            $table->foreign('historial_id')
                ->references('id')
                ->on('historiales')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->unsignedBigInteger('empleado_id');
            $table->foreign('empleado_id')
                ->references('id')
                ->on('empleados')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->unsignedBigInteger('medicina_id');
            $table->foreign('medicina_id')
                    ->references('id')
                    ->on('medicinas')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tratamientos');
    }
}
