<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class EmpleadoFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nombre'=> $this->faker->firstName(),
            'apellido'=> $this->faker->lastName(),
            'ci'=> $this->faker->randomNumber($nbDigits = 9),
            'fecha_nacimiento'=> $this->faker->date(),
            'direccion'=> $this->faker->address(),
            'telefono'=> $this->faker->phoneNumber(),
            'especialidad' => $this->faker->randomElement(['Enfermero(a)','cocinero'])
        ];
    }
}
