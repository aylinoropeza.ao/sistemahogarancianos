<?php

namespace Database\Factories;

use App\Models\Empleado;
use App\Models\Historiale;
use App\Models\Medicina;
use App\Models\Tratamiento;
use Illuminate\Database\Eloquent\Factories\Factory;

class TratamientoFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'descripcion' => $this->faker->text($maxNbChars = 25),
            'hora' => $this->faker->time(),
            'fecha' => $this->faker->dateTimeBetween('-2 week'),
            'dosis' => $this->faker->randomElement(['doble','normal']),

            'historial_id' => Historiale::all()->random()->id,
            'empleado_id'=> Empleado::all()->random()->id,
            'medicina_id'=> Medicina::all()->random()->id
        ];
    }
}

