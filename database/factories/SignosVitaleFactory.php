<?php

namespace Database\Factories;

use App\Models\Historiale;
use Illuminate\Database\Eloquent\Factories\Factory;

class SignosVitaleFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'fecha' => $this->faker->date(),
            'hora'=> $this->faker->time(),
            'P/A' => $this->faker->numberBetween($min = 70, $max = 100),
            'F/C'=> $this->faker->numberBetween($min = 60, $max = 90),
            'T'=> $this->faker->randomFloat($nbMaxDecimals = 2, $min = 36, $max = 40),
            //'historial_id' =>Historiale::factory(),
            'historial_id' =>Historiale::all()->random()->id,
        ];
    }
}
