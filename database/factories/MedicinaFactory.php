<?php

namespace Database\Factories;

use App\Models\Tratamiento;
use Illuminate\Database\Eloquent\Factories\Factory;

class MedicinaFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    protected static $suffixes = ["balamin", "bamol", "ban", "bax", "bazine", "begron", "bicin", "bide", "binol", "bisome", "bital"];
    public function definition()
    {
        return [
            'nombre' => $this->faker->randomElement(["balamin", "bamol", "ban", "bax", "bazine", "begron", "bicin", "bide", "binol", "bisome", "bital"]),
            'descripcion' => $this->faker->randomElement(['300 mg', '150 mg', '100 mg', '60 mL', '40 mL', '100 mL', 'jarabe']),
            'stock' => $this->faker->numberBetween($min = 5, $max = 20),
            'precio' => $this->faker->randomFloat($nbMaxDecimals = 2, $min = 5, $max = 50),
            'via' =>  $this->faker->randomElement(['oral', 'sublingual', 'topica', 'inhalatoria', 'parental'])
        ];
    }
}
