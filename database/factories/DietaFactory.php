<?php

namespace Database\Factories;

use App\Models\Historiale;
use Illuminate\Database\Eloquent\Factories\Factory;

class DietaFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'alimentos_prohibidos' => $this->faker->randomElement(['azucar carnes','azucar sal','sal','azucar','carnes','picante','picante,sal']),
            'alimentos_beneficioso' => $this->faker->randomElement(['avena ','Leche descremada avena','alimentos vitamina D ','Leche descremada alimentos vitamina D','comida integral ','avena','comida integral avena']),
            'historial_id' => Historiale::all()->random()->id,
        ];
    }
}
