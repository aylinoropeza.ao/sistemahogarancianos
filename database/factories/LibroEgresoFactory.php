<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class LibroEgresoFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'fecha' => $this->faker->dateTimeBetween('-2 week'),
            'concepto' =>  $this->faker->randomElement(["compra vivieres", "medicamnetos","reparacion vivienda","Electricidad"]),
            'comentario' => $this->faker->text($maxNbChars = 50),
            'saldo' => $this->faker->randomFloat($nbMaxDecimals = 2, $min = -5, $max = -100),
        ];
    }
}