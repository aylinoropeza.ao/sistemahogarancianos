<?php

namespace Database\Factories;

use App\Models\Residente;
use Illuminate\Database\Eloquent\Factories\Factory;

class HijoFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nombre'=> $this->faker->firstName(),
            'apellido'=> $this->faker->lastName(),
            'ci'=> $this->faker->randomNumber($nbDigits = 9),
            'telefono'=> $this->faker->date(),
            'residente_id'=> Residente::all()->random()->id,
        ];
    }
}