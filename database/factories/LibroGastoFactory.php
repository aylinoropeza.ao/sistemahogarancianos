<?php

namespace Database\Factories;

use App\Models\Residente;
use Illuminate\Database\Eloquent\Factories\Factory;

class LibroGastoFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'descripcion' => $this->faker->randomElement(["comida", "retira", "medicinas extra"]),
            'precio' => $this->faker->randomFloat($nbMaxDecimals = 2, $min = 5, $max = 50),
            'estado_pagado' =>  $this->faker->randomElement([true,false]),
            'fecha' => $this->faker->dateTimeBetween('-2 week','+2 week'),
            'residente_id' => Residente::all()->random()->id,
        ];
    }
}