<?php

namespace Database\Factories;

use App\Models\Residente;
use Illuminate\Database\Eloquent\Factories\Factory;

class HistorialeFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'descripcion_enfermedad' =>$this->faker->randomElement(['Artrosis ','Hipertensión','Problemas auditivos y visuales','Alzheimer']),
            'fecha' => $this->faker->date(),
           // 'residente_id'=> Residente::factory(),
           'residente_id'=> Residente::all()->random()->id,
           //'residente_id'=> $this->faker->unique()->numberBetween($min = 1, $max = 100),
        ];
    }
}