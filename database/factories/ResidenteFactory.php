<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class ResidenteFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nombre'=> $this->faker->firstName(),
            'apellido'=> $this->faker->lastName(),
            'ci'=> $this->faker->randomNumber($nbDigits = 9),
            'num_habitacion'=> $this->faker->numberBetween($min = 1, $max = 50),
            'fecha_nacimiento'=> $this->faker->date()
        ];
    }
}