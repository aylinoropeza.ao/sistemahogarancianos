<?php

namespace App\Http\Controllers;

use App\Models\Historiale;
use App\Models\Residente;
use App\Models\Dieta;
use App\Models\Empleado;
use App\Models\SignosVitale;
use App\Models\Tratamiento;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HistorialeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index($idResidente)
    {
    
        //$historiales=Historiale::where('residente_id','=', $idResidente)->orderby('fecha','desc')->get();
        $historiales = Historiale::with('residente')->where('residente_id','=', $idResidente)->orderby('fecha','desc')->get();
        return view('historiales.index', compact('historiales'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {   
        $residente = Residente::find($id);
        return view("historiales.create",compact('residente'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $inputs= $request->all();
        $historial = Historiale::create($inputs);
        $historial->save();
        return redirect('/residentes/'.$historial->residente_id.'/historiales');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Historiale  $historiale
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //return $id;
        
        $residente = Residente::where('id','=',$id)->get();
        $historial = Historiale::where('residente_id','=',$residente[0]->id)->get();
        #return $historial[0]->id;
       
        $dieta = Dieta::where('historial_id','=',$historial[0]->id)->get();
        $signosVitales = SignosVitale::where('historial_id','=',$historial[0]->id)->get();
        $tratamientos = Tratamiento::where('historial_id','=',$historial[0]->id)->get();
       # return $residente;
        return view('historiales.show', ['residente' => $residente, 'tratamientos' => $tratamientos]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Historiale  $historiale
     * @return \Illuminate\Http\Response
     */
    //recibe id del recidente
    public function edit($id)
    {
  
    }
    //recibe idHistorial
    public function edite($id)
    {
        $historial= Historiale::find($id);
        return view('historiales.edit', ['historial' => $historial]);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Historiale  $historiale
     * @return \Illuminate\Http\Response
     */
    //recibe idHistorial
    public function updates(Request $request, $id)
    {
        $historial= Historiale::find($id);
        
        $validated = $request->validate([
            'descripcion_enfermedad' => 'required|max:40',
            'fecha' => 'required'
        ]);
        $historial->descripcion_enfermedad = $request->get('descripcion_enfermedad');
        $historial->fecha = $request->get('fecha');
        $historial->save();
        return redirect('/residentes/'.$historial->residente_id.'/historiales');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Historiale  $historiale
     * @return \Illuminate\Http\Response
     */
    /*
    public function destroy($id)
    {
        $historial = Historiale::find($id);
        $historial->delete();
        return redirect('/residentes/'.$historial->residente_id.'/historiales');
    }*/


    public function destroye($id)
    {
        $historial = Historiale::find($id);
        $historial->delete();
        return redirect('/residentes/'.$historial->residente_id.'/historiales');
    }
    
}
