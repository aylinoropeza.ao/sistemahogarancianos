<?php

namespace App\Http\Controllers;

use App\Models\LibroEgreso;
use App\Models\LibroGasto;
use App\Models\LibroIngreso;
use App\Models\Residente;
use Barryvdh\DomPDF\Facade as PDF;
use Facade\FlareClient\Http\Response;
use Illuminate\Http\Request;

class InformeController extends Controller
{
  public function index()
  {
    $residentes = Residente::all();
    
    return view('informes.index',[
      'residentes'=>$residentes
    ]);
  }
  public function pdfIngresos()
  {
    $libroIngresos = LibroIngreso::all()->sortByDesc('fecha');
    $suma = LibroIngreso::sum("saldo");
    $pdf = PDF::loadView('pdf.libroIngresosPdf',compact('libroIngresos'), compact('suma'));
    return $pdf->stream('InformelibroIngresosPdf.pdf');
  }
  public function pdfEgresos()
  {

    $libroEgresos = LibroEgreso::all()->sortByDesc('fecha');
    $suma = LibroEgreso::sum("saldo");
    $pdf = PDF::loadView('pdf.libroEgresosPdf',compact('libroEgresos'), compact('suma'));
    return $pdf->stream('InformeLibroEgresosPdf.pdf');
  }
  public function pdfTotal()
  {
    $libroEgresos = LibroEgreso::all()->sortByDesc('fecha');
    $sumaEgreso = LibroEgreso::sum("saldo");
   
    $libroIngresos = LibroIngreso::all()->sortByDesc('fecha');
    $sumaIngreso = LibroIngreso::sum("saldo");

   # $librototales = array_merge($libroIngresos->toArray(), $libroEgresos->toArray());
    $suma =$sumaIngreso+ $sumaEgreso;
    $librototales = $libroIngresos->merge($libroEgresos);
    
    $pdf = PDF::loadView('pdf.libroTotalPdf',compact('librototales','libroEgresos','libroIngresos','suma'));
    return $pdf->stream('InformeLibroTotalPdf.pdf');
  }
  public function pdfIngresosFecha(Request $request)
  {
    $fecha_inicio = $request->fecha_inicio;
    $fecha_fin = $request->fecha_fin;
    $libroEgresos = LibroEgreso::all()->whereBetween('fecha',array($fecha_inicio,$fecha_fin))->sortByDesc('fecha');
    $sumaEgreso = LibroEgreso::whereBetween('fecha',array($fecha_inicio,$fecha_fin))->sum("saldo");
   
    $libroIngresos = LibroIngreso::all()->whereBetween('fecha',array($fecha_inicio,$fecha_fin))->sortByDesc('fecha');
    $sumaIngreso = LibroIngreso::whereBetween('fecha',array($fecha_inicio,$fecha_fin))->sum("saldo");

   # $librototales = array_merge($libroIngresos->toArray(), $libroEgresos->toArray());
    $suma =$sumaIngreso+ $sumaEgreso;
    $librototales = $libroIngresos->merge($libroEgresos);
    
    $pdf = PDF::loadView('pdf.libroTotalPdf',compact('librototales','libroEgresos','libroIngresos','suma'));
    return $pdf->stream('InformeLibroTotalPdf.pdf');
  }
  public function pdfGastosResidente(Request $request)
  {
   
    $id = $request->residente;
    $residente = Residente::find($id);
    $fecha_fin = $request->fecha_fin;
    $LibroGastos = LibroGasto::all()->where('residente_id',$id)->sortByDesc('fecha');
    $saldo = LibroGasto::where('estado_pagado',false)->where('residente_id',$id)->sum("precio");
    
    $pdf = PDF::loadView('pdf.LibroGastosPdf',compact('LibroGastos','residente','saldo'));
    return $pdf->stream('InformeLibroGastosPdf.pdf');
  }
  

}
