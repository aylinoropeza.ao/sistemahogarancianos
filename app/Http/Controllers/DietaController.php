<?php

namespace App\Http\Controllers;

use App\Models\Dieta;
use App\Models\Residente;
use Illuminate\Http\Request;
use DateTime;

class DietaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // Recibe el id del historial
    public function create($id)
    {
        $residente = Residente::join('historiales', 'historiales.residente_id', '=', 'residentes.id')
                ->where('historiales.id', $id)
                ->select('residentes.*')->get()->first();

        return view('dietas.create', compact('residente'))->with(compact('id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $dieta = new Dieta();

        $dieta->alimentos_prohibidos = $request->get('alimentos_prohibidos');
        $dieta->alimentos_beneficioso = $request->get('alimentos_beneficioso');
        $dieta->historial_id = $request->get('historial_id');
        //$dieta->historial_id = 1;

        $dieta->save();

        $residente = Residente::join('historiales', 'historiales.residente_id', '=', 'residentes.id')
            ->join('dietas', 'dietas.historial_id', '=', 'historiales.id')
                    ->where('dietas.id', $dieta->id)
                    ->select('residentes.*')->get()->first();

        return redirect('dietas/'.$residente->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // Recibe el id del residente
    public function show($id)
    {
        $residente = Residente::find($id);
        $dieta = Dieta::join('historiales', 'historiales.id', '=', 'dietas.historial_id')
            ->join('residentes', 'residentes.id', '=', 'historiales.residente_id')
                    ->where('residentes.id', $id)
                    ->select('dietas.*')->get()->first();
        $fecha = new DateTime($residente->first()->fecha_nacimiento);
        $hoy = new DateTime();
        $dif = $hoy->diff($fecha);
        $edad = $dif->y;
        return view('dietas.show', compact('dieta'))->with(compact('residente'))->with(compact('edad'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Dieta  $dieta
     * @return \Illuminate\Http\Response
     */
    public function edit(Dieta $dieta)
    {
        $residente = Residente::join('historiales', 'historiales.residente_id', '=', 'residentes.id')
            ->join('dietas', 'dietas.historial_id', '=', 'historiales.id')
                    ->where('dietas.id', $dieta->id)
                    ->select('residentes.*')->get()->first();
        return view('dietas.edit', compact('dieta'))->with(compact('residente'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Dieta  $dieta
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Dieta $dieta)
    {
        $residente = Residente::join('historiales', 'historiales.residente_id', '=', 'residentes.id')
            ->join('dietas', 'dietas.historial_id', '=', 'historiales.id')
                    ->where('dietas.id', $dieta->id)
                    ->select('residentes.*')->get()->first();
        $dieta->alimentos_prohibidos = $request->get('alimentos_prohibidos');
        $dieta->alimentos_beneficioso = $request->get('alimentos_beneficioso');
        $dieta->historial_id = $request->get('historial_id');

        $dieta->save();

        return redirect('dietas/'.$residente->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Dieta  $dieta
     * @return \Illuminate\Http\Response
     */
    public function destroy(Dieta $dieta)
    {
        $residente = Residente::join('historiales', 'historiales.residente_id', '=', 'residentes.id')
            ->join('dietas', 'dietas.historial_id', '=', 'historiales.id')
                    ->where('dietas.id', $dieta->id)
                    ->select('residentes.*')->get()->first();
        //dd($dieta);
        //dd($residente);
        $dieta->delete();
        return redirect('dietas/'.$residente->id);
    }
}
