<?php

namespace App\Http\Controllers;

use App\Models\Medicina;
use Illuminate\Http\Request;

class MedicinaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $medicinas=Medicina::all();
        return view('medicinas.index', compact('medicinas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $vias = ['oral', 'sublingual', 'topica', 'inhalatoria', 'parental'];
        return view('medicinas.create', compact('vias'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'nombre' => 'required|string|max:30',
            'descripcion' => 'required|string',
            'stock' => 'required|integer',
            'precio' => 'required|numeric|min:0',
            'via' => 'required|in:oral,sublingual,topica,inhalatoria,parental',
        ]);

        $medicinas = new Medicina();

        $medicinas->nombre = $request->get('nombre');
        $medicinas->descripcion = $request->get('descripcion');
        $medicinas->stock = $request->get('stock');
        $medicinas->precio = $request->get('precio');
        $medicinas->via = $request->get('via');

        $medicinas->save();

        return redirect('/medicinas');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Medicina  $medicina
     * @return \Illuminate\Http\Response
     */
    public function show(Medicina $medicina)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Medicina  $medicina
     * @return \Illuminate\Http\Response
     */
    public function edit(Medicina $medicina)
    {
        $vias = ['oral', 'sublingual', 'topica', 'inhalatoria', 'parental'];
        return view('medicinas.edit', compact('medicina'))->with(compact('vias'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Medicina  $medicina
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Medicina $medicina)
    {
        $validated = $request->validate([
            'nombre' => 'required|string|max:30',
            'descripcion' => 'required|string',
            'stock' => 'required|integer',
            'precio' => 'required|numeric|min:0',
            'via' => 'required|in:oral,sublingual,topica,inhalatoria,parental',
        ]);
        
        $medicina->nombre = $request->get('nombre');
        $medicina->descripcion = $request->get('descripcion');
        $medicina->stock = $request->get('stock');
        $medicina->precio = $request->get('precio');
        $medicina->via = $request->get('via');
        $medicina->save();

        return redirect('/medicinas');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Medicina  $medicina
     * @return \Illuminate\Http\Response
     */
    public function destroy(Medicina $medicina)
    {
        $medicina->delete();
        return redirect('/medicinas');
    }
}
