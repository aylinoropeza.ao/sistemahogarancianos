<?php

namespace App\Http\Controllers;

use App\Models\Residente;
use App\Models\SignosVitale;
use Barryvdh\DomPDF\Facade as PDF;
use App\Models\Hijo;

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ResidenteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $residentes=Residente::withCount(['hijos'])->get();
        return view('residentes.index',[
            'residentes'=>$residentes
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('residentes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'nombre' => 'required|max:30',
            'apellido' => 'required|max:40',
            'ci' => 'required|unique:residentes|integer',
            'num_habitacion' => 'required',
            'fecha_nacimiento' => 'required'
        ]);
        $residente = new Residente();
        $residente->nombre = $request->get('nombre');
        $residente->apellido = $request->get('apellido');
        $residente->ci = $request->get('ci');
        $residente->num_habitacion = $request->get('num_habitacion');
        $residente->fecha_nacimiento = $request->get('fecha_nacimiento');

        if($request->hasFile("imagen")){
            $imagen = $request->file("imagen");
            $nombreimagen = Str::slug($request->nombre).".".$imagen->guessExtension();
            $ruta = public_path("img/residente/");
            //$imagen->move($ruta,$nombreimagen);
            copy($imagen->getRealPath(),$ruta.$nombreimagen);

            $residente->imagen = $nombreimagen;            
            
        }
        $residente->save();

        return view('hijos.create')->with('residente', $residente);
        //return redirect('/residentes/'.$residente->id.'/hijos/create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $residente = Residente::find($id);
        //$residentehijos = Residente::join('hijos','hijos.residente_id','=','residentes.id')->get();
        $hijos = DB::table('hijos')->where('residente_id',$id)->get();
        return view('residentes.show',[
            'residente'=>$residente,'hijos'=>$hijos
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $residente = Residente::find($id);
        $hijos = Hijo::select('hijos.*')->where('hijos.residente_id', $id);
        return view('residentes.edit')->with('residente', $residente)->with('hijos', $hijos);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validated = $request->validate([
            'nombre' => 'required|max:30',
            'apellido' => 'required|max:40',
            'ci' => 'required|integer',
            'num_habitacion' => 'required',
            'fecha_nacimiento' => 'required'
        ]);
        $residente = Residente::find($id);

        $residente->nombre = $request->get('nombre');
        $residente->apellido = $request->get('apellido');
        $residente->ci = $request->get('ci');
        $residente->num_habitacion = $request->get('num_habitacion');
        $residente->fecha_nacimiento = $request->get('fecha_nacimiento');
        if($request->hasFile("imagen")){
            $imagen = $request->file("imagen");
            $nombreimagen = Str::slug($request->nombre).".".$imagen->guessExtension();
            $ruta = public_path("img/residente/");
            //$imagen->move($ruta,$nombreimagen);
            copy($imagen->getRealPath(),$ruta.$nombreimagen);

            $residente->imagen = $nombreimagen;            
            
        }
        $residente->save();

        return redirect('/residentes/'.$residente->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $residente = Residente::find($id);
        $residente->delete();
        return redirect('/residentes');
    }

    public function getPdfSignosVitales($id){
       
        $signosVitales = SignosVitale::join('historiales', 'signos_vitales.historial_id', 'historiales.id')
            ->join('residentes', 'historiales.residente_id', 'residentes.id')
            ->where('residente_id', $id)
            ->select('signos_vitales.P/A as presionArterial','signos_vitales.F/C as frecuenciaCardiaca','signos_vitales.fecha as fecha','signos_vitales.hora as hora','signos_vitales.T as temperatura','residentes.*')
            ->orderBy('fecha')
            ->orderBy('hora')
            //->orderBy('cantidad_citas', 'desc')
            ->get();
        //$signosVitales = SignosVitale::get();
      #  $pdf = PDF::loadView('pdf.residentesSignosVitales',compact('signosVitales'));
       $pdf = PDF::loadView('pdf.residentesSignosVitales',compact('signosVitales'));
        #return $signosVitales;
        return $pdf->stream('signosVitales.pdf');
    }
  
}