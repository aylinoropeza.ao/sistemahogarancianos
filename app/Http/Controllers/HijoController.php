<?php

namespace App\Http\Controllers;

use App\Models\Hijo;
use App\Models\Residente;

use Illuminate\Http\Request;

class HijoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'nombre' => 'required|max:30',
            'apellido' => 'required|max:40',
            'ci' => 'required|unique:residentes|integer',
            'telefono' => 'required|integer',
        ]);
        $hijo = new Hijo();
        $hijo->nombre = $request->get('nombre');
        $hijo->apellido = $request->get('apellido');
        $hijo->ci = $request->get('ci');
        $hijo->telefono = $request->get('telefono');
        $hijo->residente_id = $request->get('residente_id');
        $hijo->save();

        //return view('hijos.create')->with('residente', $residente);
        return redirect('/residentes/'.$hijo->residente_id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $residente = Residente::find($id);
        return view('hijos.create')->with('residente', $residente);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $hijo = Hijo::find($id);
        return view('hijos.edit')->with('hijo', $hijo);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validated = $request->validate([
            'nombre' => 'required|max:30',
            'apellido' => 'required|max:40',
            'ci' => 'required|unique:residentes|integer',
            'telefono' => 'required|integer',
        ]);
        $hijo = Hijo::find($id);
        
        $hijo->nombre = $request->get('nombre');
        $hijo->apellido = $request->get('apellido');
        $hijo->ci = $request->get('ci');
        $hijo->telefono = $request->get('telefono');
        $hijo->save();

        return redirect('/residentes/'.$hijo->residente_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $hijo = Hijo::find($id);
        $hijo->delete();
        return redirect('/residentes/'.$hijo->residente_id);
    }
}