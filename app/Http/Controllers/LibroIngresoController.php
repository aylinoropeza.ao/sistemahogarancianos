<?php

namespace App\Http\Controllers;

use App\Models\LibroIngreso;
use Illuminate\Http\Request;

class LibroIngresoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $libroIngresos = libroIngreso::orderby('fecha','desc')
        ->select('*')
        ->get();
        
        return view('libroIngresos.index', compact('libroIngresos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tipos =['normal', 'donacion'];
        return view("libroIngresos.create",compact('tipos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $inputs= $request->all();
        $libroIngreso = libroIngreso::create($inputs);
        $libroIngreso->save();
        return redirect('/libroIngresos');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\LibroIngreso  $libroIngreso
     * @return \Illuminate\Http\Response
     */
    public function show(LibroIngreso $libroIngreso)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\LibroIngreso  $libroIngreso
     * @return \Illuminate\Http\Response
     */
    public function edit(LibroIngreso $libroIngreso)
    {
        $tipos = ['normal', 'donacion'];
        return view('libroIngresos.edit', compact('libroIngreso'))->with(compact('tipos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\LibroIngreso  $libroIngreso
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LibroIngreso $libroIngreso)
    {
        #$libroIngreso = LibroIngreso::find($request->input('id'));

       # $libroIngreso->fill($request->input());
       # $libroIngreso->saveOrFail();
       /*
       $libroIngreso->nombre = $request->get('fecha');
        $libroIngreso->descripcion = $request->get('concepto');
        $libroIngreso->stock = $request->get('stock');
        $libroIngreso->precio = $request->get('saldo');
        $libroIngreso->via = $request->get('tipo');
        $libroIngreso->save();
*/
        $libroIngreso->update($request->all());
        return redirect('/libroIngresos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\LibroIngreso  $libroIngreso
     * @return \Illuminate\Http\Response
     */
    public function destroy(LibroIngreso $libroIngreso)
    {
        $libroIngreso->delete();
        return redirect('libroIngresos');
    }
}
