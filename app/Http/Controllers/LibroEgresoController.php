<?php

namespace App\Http\Controllers;

use App\Models\LibroEgreso;
use Illuminate\Http\Request;

class LibroEgresoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $libroEgresos = LibroEgreso::orderby('fecha','desc')
        ->select('*')
        ->get();
        
        return view('libroEgresos.index', compact('libroEgresos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("libroEgresos.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $inputs= $request->all();
        $libroEgreso = libroEgreso::create($inputs);
        $libroEgreso->save();
        return redirect('/libroEgresos');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\LibroEgreso  $libroEgreso
     * @return \Illuminate\Http\Response
     */
    public function show(LibroEgreso $libroEgreso)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\LibroEgreso  $libroEgreso
     * @return \Illuminate\Http\Response
     */
    public function edit(LibroEgreso $libroEgreso)
    {
        return view('libroEgresos.edit', compact('libroEgreso'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\LibroEgreso  $libroEgreso
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LibroEgreso $libroEgreso)
    {
        $libroEgreso->update($request->all());
        return redirect('/libroEgresos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\LibroEgreso  $libroEgreso
     * @return \Illuminate\Http\Response
     */
    public function destroy(LibroEgreso $libroEgreso)
    {
        $libroEgreso->delete();
        return redirect('/libroEgresos');
    }
}
