<?php

namespace App\Http\Controllers;

use App\Models\Tratamiento;
use App\Models\Medicina;
use App\Models\Empleado;
use App\Models\Residente;
use Illuminate\Http\Request;
use DateTime;

class TratamientoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tratamientos=Tratamiento::all();
        //return view('tratamientos.index', compact('tratamientos'));
        return redirect('/home');
    }

    /**
     * Show the form for creating a new resource.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // Recibe el id del historial
    public function create($id)
    {
        $residente = Residente::join('historiales', 'historiales.residente_id', '=', 'residentes.id')
                ->where('historiales.id', $id)
                ->select('residentes.*')->get()->first();
        $medicinas=Medicina::all();
        $empleados=Empleado::all();
        return view('tratamientos.create', compact('medicinas'))->with(compact('empleados'))->with(compact('id'))->with(compact('residente'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'descripcion' => 'required|string',
            'fecha' => 'required|date',
            'dosis' => 'required|max:20'
        ]);

        $tratamientos = new Tratamiento();

        $tratamientos->descripcion = $request->get('descripcion');
        $tratamientos->hora = $request->get('hora');
        $tratamientos->fecha = $request->get('fecha');
        $tratamientos->dosis = $request->get('dosis');
        $tratamientos->historial_id = $request->get('historial_id');
        //$tratamientos->historial_id = 1;
        $tratamientos->empleado_id = $request->get('empleado_id');
        $tratamientos->medicina_id = $request->get('medicina_id');

        $tratamientos->save();

        $residente = Residente::join('historiales', 'historiales.residente_id', '=', 'residentes.id')
            ->join('tratamientos', 'tratamientos.historial_id', '=', 'historiales.id')
                    ->where('tratamientos.id', $tratamientos->id)
                    ->select('residentes.*')->get()->first();

        return redirect('/tratamientos/'.$residente->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // recibe el id del residente
    public function show($id)
    {
        $residente = Residente::find($id);
        $tratamientos = Tratamiento::join('historiales', 'historiales.id', '=', 'tratamientos.historial_id')
            ->join('residentes', 'residentes.id', '=', 'historiales.residente_id')
                    ->where('residentes.id', $id)
                    ->select('tratamientos.*')->get();
        $medicina = Tratamiento::join('medicinas', 'medicinas.id', '=', 'tratamientos.medicina_id')
                ->where('tratamientos.id', $tratamientos->first()->id)
                ->select('medicinas.*')->get()->first();
        $fecha = new DateTime($residente->fecha_nacimiento);
        $hoy = new DateTime();
        $dif = $hoy->diff($fecha);
        $edad = $dif->y;
        return view('tratamientos.show', compact('tratamientos'))->with(compact('residente'))->with(compact('medicina'))->with(compact('edad'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Tratamiento  $tratamiento
     * @return \Illuminate\Http\Response
     */
    public function edit(Tratamiento $tratamiento)
    {
        $medicinas=Medicina::all();
        $empleados=Empleado::all();
        $residente = Residente::join('historiales', 'historiales.residente_id', '=', 'residentes.id')
            ->join('tratamientos', 'tratamientos.historial_id', '=', 'historiales.id')
                    ->where('tratamientos.id', $tratamiento->id)
                    ->select('residentes.*')->get()->first();
        return view('tratamientos.edit', compact('tratamiento'))->with(compact('medicinas'))->with(compact('empleados'))->with(compact('residente'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Tratamiento  $tratamiento
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tratamiento $tratamiento)
    {
        //dd($tratamiento->empleado_id);
        $validated = $request->validate([
            'descripcion' => 'required|string',
            'fecha' => 'required|date',
            'dosis' => 'required|max:20',
        ]);

        $tratamiento->descripcion = $request->get('descripcion');
        $tratamiento->hora = $request->get('hora');
        $tratamiento->fecha = $request->get('fecha');
        $tratamiento->dosis = $request->get('dosis');
        $tratamiento->historial_id = $request->get('historial_id');
        $tratamiento->empleado_id = $request->get('empleado_id');
        $tratamiento->medicina_id = $request->get('medicina_id');

        $tratamiento->save();

        $residente = Residente::join('historiales', 'historiales.residente_id', '=', 'residentes.id')
            ->join('tratamientos', 'tratamientos.historial_id', '=', 'historiales.id')
                    ->where('tratamientos.id', $tratamiento->id)
                    ->select('residentes.*')->get()->first();

        return redirect('tratamientos/'.$residente->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Tratamiento  $tratamiento
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tratamiento $tratamiento)
    {
        $residente = Residente::join('historiales', 'historiales.residente_id', '=', 'residentes.id')
            ->join('tratamientos', 'tratamientos.historial_id', '=', 'historiales.id')
                    ->where('tratamientos.id', $tratamiento->id)
                    ->select('residentes.*')->get()->first();
        $tratamiento->delete();
        return redirect('tratamientos/'.$residente->id);
    }
}
