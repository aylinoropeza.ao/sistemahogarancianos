<?php

namespace App\Http\Controllers;

use App\Models\LibroGasto;
use App\Models\Residente;
use Illuminate\Http\Request;

class LibroGastoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$libro_gastos = libroGasto::orderby('fecha','desc')->simplePaginate(15);
        $libro_gastos = libroGasto::simplePaginate(10);
        
        return view('libroGastos.index', compact('libro_gastos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $residentes = Residente::all();
        return view('libroGastos.create', compact('residentes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'descripcion' => 'required|string',
            'precio' => 'required|numeric|min:0',
            'estado_pagado' => 'required|boolean',
            'fecha' => 'required',
            'residente_id' => 'required',
        ]);

        $libroGasto = new LibroGasto();

        $libroGasto->descripcion = $request->get('descripcion');
        $libroGasto->precio = $request->get('precio');
        $libroGasto->estado_pagado = $request->get('estado_pagado');
        $libroGasto->fecha = $request->get('fecha');
        $libroGasto->residente_id = $request->get('residente_id');

        $libroGasto->save();

        return redirect('/libroGastos');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $libro_gastos = LibroGasto::where('residente_id','=',$id)->orderby('fecha','desc')->paginate(10);
        return view('libroGastos.show', compact('libro_gastos'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\LibroGasto  $libroGasto
     * @return \Illuminate\Http\Response
     */
    public function edit(LibroGasto $libroGasto)
    {
        $residentes = Residente::all();
        return view('libroGastos.edit', compact('libroGasto'))->with(compact('residentes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\LibroGasto  $libroGasto
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LibroGasto $libroGasto)
    {
        $validated = $request->validate([
            'descripcion' => 'required|string',
            'precio' => 'required|numeric|min:0',
            'estado_pagado' => 'required|boolean',
            'fecha' => 'required',
            'residente_id' => 'required',
        ]);
        
        $libroGasto->descripcion = $request->get('descripcion');
        $libroGasto->precio = $request->get('precio');
        $libroGasto->estado_pagado = $request->get('estado_pagado');
        $libroGasto->fecha = $request->get('fecha');
        $libroGasto->residente_id = $request->get('residente_id');

        $libroGasto->save();

        return redirect('/libroGastos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\LibroGasto  $libroGasto
     * @return \Illuminate\Http\Response
     */
    public function destroy(LibroGasto $libroGasto)
    {
        $libroGasto->delete();
        return redirect('/libroGastos');
    }
}
