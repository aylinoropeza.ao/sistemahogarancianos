<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SignosVitale extends Model
{
    use HasFactory;

    //Relación uno a muchos (Inverso)
    public function historiale(){
        return $this->belongsTo('App\Models\Historiale');
    }
}
