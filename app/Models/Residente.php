<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Residente extends Model
{
    use HasFactory;

    //Relación uno a muchos
    public function hijos()
    {
        return $this->hasMany('App\Models\Hijo');
    }
    //Relación uno a muchos
    public function historiales()
    {
        return $this->hasMany('App\Models\Historiale');
    }
    //Relación uno a muchos
    public function libroGastos()
    {
        return $this->hasMany('App\Models\LibroGasto');
    }
}
