<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Hijo extends Model
{
    use HasFactory;

    //Relación uno a muchos (Inverso)
    public function residente(){
        return $this->belongsTo('App\Models\Residente');
    }
}
