<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LibroGasto extends Model
{
    use HasFactory;

    //Relación uno a muchos(inverso)
    public function residente()
    {
        return $this->belongsTo('App\Models\Residente');
    }
}
