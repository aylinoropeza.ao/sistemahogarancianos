<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tratamiento extends Model
{
    use HasFactory;
    //Relación uno a muchos(inverso)
    public function historiale(){
        return $this->belongsTo('App\Models\Historiale');
    }
    //Relación uno a muchos(inverso)
     public function medicina(){
        return $this->belongsTo('App\Models\Medicina');
    }
    //Relación uno a muchos(inverso)
    public function empleado()
    {
        return $this->belongsTo('App\Models\Empleado');
    }
}
