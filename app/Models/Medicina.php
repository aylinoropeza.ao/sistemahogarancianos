<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Medicina extends Model
{
    use HasFactory;

    //Relación uno a muchos 
    public function tratamientos(){
        return $this->hasMany('App\Models\Tratamiento');
    }
    
}
