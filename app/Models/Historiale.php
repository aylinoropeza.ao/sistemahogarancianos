<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Historiale extends Model
{
    use HasFactory;

    protected $primaryKey = 'id';

    protected $fillable = [
        'descripcion_enfermedad',
        'fecha',
        'residente_id'
    ];
    /*
    public function residente():BelongsTo{
        return $this->belongsTo(Residente::class,'residente_id','id');
    }*/

    //Relación uno a muchos(inverso)
    public function residente(){
        return $this->belongsTo('App\Models\Residente');
    }
    //Relación uno a uno
    public function dieta()
    {
        return $this->hasOne('App\Models\Dieta');
    }
    //Relación uno a muchos
    public function signosVitales(){
        return $this->hasMany('App\Models\SignosVitale');
    }
    //Relación uno a muchos
    public function tratamientos(){
        return $this->hasMany('App\Models\Tratamiento');
    }
}
